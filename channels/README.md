# Channels

Video url: https://www.youtube.com/watch?v=b4mS5UPHh20

## `std::sync::mpsc::channel`

https://doc.rust-lang.org/std/sync/mpsc/index.html

```rust
pub fn channel<T>() -> (Sender<T>, Receiver<T>)
```

You can create a channel for almost anything.

If `T` does not implement `Send`, `Sender` does not implement it either.
Therefore you can construct a channel for items that can't be sent as long as
you don't move the `Sender` or `Receiver` accross a thread boundary.

`T` has to be `Sized` because the trait is implied if the boundary doesn't
explicitly state `T: ?Sized`.

The channel type itself owns the data.

## `Condvar`

`Condvar`, a conditional variable is a way to announce to a different thread
that you have changed something it cares about.

## Structure of our channel implementation

An `Inner` type is often used in rust when we want multiple things to share some
data.

```rust
use std::sync::{Arc, Condvar, Mutex};

pub struct Sender<T> {
    inner: Arc<Inner<T>>,
}

pub struct Receiver<T> {
    inner: Arc<Inner<T>>,
}

struct Inner<T> {
    queue: Mutex<Vec<T>>,
}

pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let inner = Inner { queue: Vec::new() };
    let inner = Arc::new(Mutex::new(inner));
    (
        Sender {
            inner: inner.clone(),
        },
        Receiver {
            inner: inner.clone(),
        },
    )
}
```

When you call `lock` on a mutex, it returns a `LockResult` which can also signal
you that another thread left the data in an inconsistent state, for example by
panicking (the lock was unlocked due to the panic). In that case the
`LockResult` is a `PoisonError`.

Now add implementations for `Sender` and `Receiver`

```rust
impl<T> Sender<T> {
    pub fn send(&mut self, t: T) {
        let queue = self.inner.queue.lock().unwrap();
        queue.push(t);
    }
}

pub struct Receiver<T> {
    inner: Arc<Inner<T>>,
}

impl<T> Receiver<T> {
    pub fn recv(&mut self) -> T {
        let queue = self.inner.queue.lock().unwrap();
        queue.pop()
    }
}
```

The issue here is that the receiver gets the last thing that was sent (like
popping a stack). We could remove the first element from the vector, but what it
ends up doing is shifting all the other elements left which is not ideal.

The ideal solution would be a ring buffer.

We will use a `VecDeque` which is like a vector but it keeps track of the start
and end positions of the vector. If you push and pop on it repeatedly the start
and end pointers migrate forward.

Next we will fix the receive method to block the thread until there is data.
A `try_recv -> Option<T>` could be useful too. Here is where the `Condvar` comes
in. When `wait` returns it re-acquires the lock for us.

```rust
struct Inner<T> {
    queue: Mutex<VecDeque<T>>,
    available: Condvar,
}
```

The `Condvar` is outside the `Mutex` because when you notify the receiver, you
cannot have the lock locked, otherwise you end up in a deadlock.

The `Condvar` has a `wait` method which takes in a mutex and waits for the
notification. The idea of `wait` is that you can't be holding the mutex while
waiting.

```rust
pub fn wait<'a, T>(
    &self,
    guard: MutexGuard<'a, T>
) -> LockResult<MutexGuard<'a, T>>
```

```rust
// impl Receiver
pub fn recv(&mut self) -> T {
    let mut queue = self.inner.queue.lock().unwrap();
    loop {
        match queue.pop_front() {
            Some(t) => return t,
            None => {
                queue = self.inner.available.wait(queue).unwrap();
            }
        }
    }
}

// impl Sender
pub fn send(&mut self, t: T) {
    let queue = self.inner.queue.lock().unwrap();
    queue.push_back(t);
    drop(queue); // drop the lock
    self.inner.available.notify_one();
}
```

The loop is required because the operating system might wake the thread up even
though the queue is still empty.

We also want to make the `Sender` clonable. Unfortunately

```rust
#[derive(Clone)]
```

Won't work as it desugars into

```rust
impl<T: Clone> Clone for Sender<T> {
    fn clone(&self) -> Self {
        // ...
    }
}
```

Which clones the `T` not the `Arc`!

What we want is to implement it ourselves

```rust
impl<T> Clone for Sender<T> {
    fn clone(&self) -> Self {
        Sender {
            inner: self.inner.clone()
        }
    }
}
```

But this is unstable because `Arc` is a smart pointer and the compiler doesn't
know if you want to clone the pointer or the `Inner` in this case (due to
dereffing).

```rust
// fn clone
Sender {
    inner: Arc::clone(&self.inner)
}
```

Next problem is how do we close the channel once all senders are dead. The
following test hangs

```rust
#[test]
fn closed() {
    let (tx, mut rx) = channel::<()>();
    let _ = tx;
    let _ = rx.recv();
}
```

`Arc::strong_count` tells you how many references there are to that arc (how
many clones). If there's only one, it must be the one of the receiver's and
there are no senders left.

The `dbg!` macro can be used for quick and dirty printing of a value.

## Synchronous channels

There exist two kinds of channel implementations: synchronous and asynchronous.
They are not to be confused with async.

Synchronous channel forces the senders and receivers to synchronize. If a sender
is sending data much faster than the receiver could consume it the queue would
grow indefinitely, but if you have a synchronous channel the channel has
a limited capacity and once the sender has filled that queue, it starts blocking
until the receiver consumes an item from it.

The primary difference between synchronous and asynchronous channel is whether
sends can block. In our implementation sends can't block.

Synchronous channels create some additional challenges, because the receiver
needs to notify the sender. In practice this requires two `Condvar`s.

`std::sync::mpsc::sync_channel` takes a bound capacity parameter but
a `mpsc::channel` doesn't.

`Weak` is a version of `Arc` that doesn't increment its reference count.

## Batch recv optimization

We can add a buffer to the receiver to store all the data from the channel to
make the code faster by not using the mutex as often.

```rust
pub struct Receiver<T> {
    shared: Arc<Shared<T>>,
    buffer: VecDeque<T>
}

impl<T> Receiver<T> {
    pub fn recv(&mut self) -> Option<T> {
        if let Some(t) = self.buffer.pop_front() {
            return Some(t);
        }

        let mut inner = self.shared.inner.lock().unwrap();
        loop {
            match inner.queue.pop_front() {
                Some(t) => {
                    if !inner.queue.is_empty() {
                        std::mem::swap(&mut self.buffer, &mut inner.queue);
                    }
                    return Some(t)
                },
                None if inner.senders == 0 => return None,
                None => {
                    inner = self.shared.available.wait(inner).unwrap();
                }
            }
        }
    }
}
```

It does require the amount of memory.

## Flavors

- Synchronous channels
  - Channel where `send()` can block
  - Limited capacity
  - Implementation options:
    - `Mutex + Condvar + VecDeque`
    - `Atomic VecDeque (atomic queue) + thread::park + thread::Thread::notify`
- Asynchronous channels
  - `send()` cannot block
  - Unbounded
  - Implementation options:
    - `Mutex + Condvar + VecDeque`
    - `Mutex + Condvar + LinkedList` (never have to resize)
    - `Atomic linked list of T`
    - `Atomic block linked list of atomic VecDeque<T>`
- Rendevouz channels
  - Synchronous channel with capacity 0
  - Used commonly for thread synchronization
- Oneshot channels
  - Only one call to `send()`
  - Any capacity
  - Might be used to transfer a kill signal

These flavors are different enough to have different implementations be
advantageous.

<!-- vim: set sw=4 ts=4 tw=80 : -->
