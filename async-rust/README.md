# The What and How of Futures and async/await in Rust

Video url: https://www.youtube.com/watch?v=9_3krAQtD2k

## What are futures?

Every future (trait) has a `poll` method

https://docs.rs/futures/0.1.25

```rust
fn poll(self: &mut Self) -> Poll<Self::Item, Self::Error>
```

A `Poll` is a result

```rust
type Poll<T, E> = Result<Async<T>, E>
```

An `Async` is ready or not ready

```rust
pub enum Async<T> {
  Ready(T),
  NotReady,
}
```

## How are futures executed?

An executor is something which makes sure futures get done.

An example pseudo-rust of an executor: (but all futures must have the same item)

```rust
struct Executor;

impl Executor {
  /// Polls futures in a loop and returns the values once every future has resolved
  fn run_all(&mut self, futures: Vec<Future>) -> Vec<(usize, Result<Future::Item, Future::Error>)> {
    let mut done = 0;
    let mut results = Vec::with_capacity(futures.len());
    while done != futures.len() {
      for (i, f) in futures.iter_mut().enumerate() {
        // Calling poll in a loop is inefficient
        match f.poll() {
          Ok(Async::Ready(t)) => {
            results.push(i, Ok(t));
            done += 1;
          }
          Err(e) => {
            results.push(i, Err(e));
            done += 1;
          }
          Ok(Async::NotReady) => {
            continue;
          }
        }
      }
    }

    results
  }
}

fn main() {
  // x and y are futures

  let a: Executor;
  let results = a.run_all(vec![x, y]);
}
```

## Tasks

When a future is not ready, calling poll will register interest of the current task in the value
being produced. When the future is ready to make progress (makes progress when polled) the `unpark`
method is called.

When polling a future returns NotReady, it means the future is not ready to make progress

The future "tells" another thread that it can be polled again once some time has passed

https://docs.rs/futures/0.1.25/futures/task/index.html

when you call poll, it can call `task::current` to get a handle to the current executor or more
specifically a _task_.
The returned task is "sent" to another thread to call `notify` on the task at some point.

Calling notify is like saying you can now make progress. It does not mean the future is ready

The executor pseudo-rust

```rust
  // ...

  fn run_all(&mut self, futures: Vec<Future>) -> Vec<(usize, Result<Future::Item, Future::Error>)> {
    let mut done = 0;
    let mut results = Vec::with_capacity(futures.len());
    while done != futures.len() {
      for (i, f) in futures.iter_mut().enumerate() {
        match f.poll() {
          Ok(Async::Ready(t)) => {
            results.push(i, Ok(t));
            done += 1;
          }
          Err(e) => {
            results.push(i, Err(e));
            done += 1;
          }
          Ok(Async::NotReady) => {
            // meanwhile...
            // another thread notices that a network packet arrived
            // which the future `f` is interested in
            continue;
          }
        }
      }

      // wait for Task::notify to be called
    }

    results
  }
```

Think of `f` like a tcp connection. The thread of `f.poll()` gave its task handle to some
other thread, so that thread knows when something interesting comes in, I have to wake up
that task with `task.notify()`. The notify is going to unpark the parked thread

Instead of having one task for all futures we can have a more sophisticated executor
with one task per future:

```rust
  // ...

  fn run_all(&mut self, futures: Vec<Future>) -> Vec<(usize, Result<Future::Item, Future::Error>)> {
    let mut done = 0;
    let mut results = Vec::with_capacity(futures.len());
    let mut tasks = Vec::with_capacity(futures.len());
    for _ in 0..futures.len() {
      tasks.push(Task::new())
    }

    while done != futures.len() {
      for (i, f) in futures.iter_mut().enumerate() {
        // don't poll futures that can't make progress
        if !tasks[i].notified() {
          continue;
        }
        task::set_current(tasks[i]);
        // Now if f.poll() tries to get a handle to itself using task::current
        // it will get the future's task, tasks[i], which it can give to another thread
        // which can call `notify` later
        match f.poll() {
          Ok(Async::Ready(t)) => {
            results.push(i, Ok(t));
            done += 1;
          }
          Err(e) => {
            results.push(i, Err(e));
            done += 1;
          }
          Ok(Async::NotReady) => {
            // f must have arranged its task `tasks[i]` to notify later
            continue;
          }
        }
      }

      // wait for Task::notify to be called
    }

    results
  }
```

In short, the notion of a task is something that knows when a future can make progress

If the task `f.poll()` gives to a thread is never notified, the future is never polled again,
thus cannot never resolve. This is why part of the _contract_ for poll (if you implement a
future) is that it must have arranged for yourself to wake up again.

Now the executor's thread sleeps whenever there's no reason to poll futures making
the executor more efficient.

The tricky part of this system is to notify the task. (the futures crate is not concerned
in this)

In the crate `futures` `futures::executor` the `NotifyHandle` can be thought of like a task.

https://docs.rs/futures/0.1.25/futures/executor/fn.with_notify.html

Fitting our pseudo-rust with the actual `futures` crate, we get something like

```rust
struct Executor(Arc<Mutex<Vec<bool>>>);

struct MyNotifier(Mutex<Vec<bool>>);

// Notify trait from futures::executor
pub trait Notify: Send + Sync {
    fn notify(&self, id: usize);
    fn clone_id(&self, id: usize) -> usize { ... }
    fn drop_id(&self, id: usize) { ... }
}

impl Notify for MyNotifier {
  fn notify(&self, id: usize) {
    self.0.lock()[id] = true;
  }
}

impl Executor {
  fn run_all(&mut self, futures: Vec<Future>) -> Vec<(usize, Result<Future::Item, Future::Error>)> {
    let mut done = 0;
    let mut results = Vec::with_capacity(futures.len());
    let nf = Arc::new(MyNotifier(Mutex::new(vec![true; futures.len()])));
    let notifier = NotifyHandle::from(nf);

    while done != futures.len() {
      for (i, f) in futures.iter_mut().enumerate() {
        // don't poll futures that can't make progress
        let was_notified = self.0.lock();
        if !was_notified[i] {
          continue;
        }
        // "reset" the notify so that it will poll only once the executor is notified
        was_notified[i] = false;
        drop(was_notified);

        // id: i is an identifier for the current future
        match executor::with_notify(&notifier, i, || f.poll()) {
          // task::current() -> Task
          // let t: Task;
          // t.notify() calls the notify method for the MyNotifier instance with the id
          Ok(Async::Ready(t)) => {
            results.push(i, Ok(t));
            done += 1;
          }
          Err(e) => {
            results.push(i, Err(e));
            done += 1;
          }
          Ok(Async::NotReady) => {
            continue;
          }
        }
      }
    }

    results
  }
}
```

All this infrastructure is for is that when you call poll you can call notify on that handle.

The question is who calls notify on that handle?

## How do you integrate futures with I/O?

`futures` doesn't define any implementation details, only the framework/interface.
It doesn't provide an executor, doesn't provide that _other_ thread that should wake you up.

`tokio` is a crate which fills that purpose. It is an implementation of the executor and
other infrastructure that is needed in order to make things wake up.

In tokio the thing that wakes you up is called a `reactor`

https://docs.rs/tokio/0.1.22/tokio/reactor/index.html

The most common thing that wakes you up is io, finished disk read/write or network read/write etc.

With the kernel, all io is identified using a number. When calling connect() for example
the returned value is the id of the connection https://man7.org/linux/man-pages/man2/connect.2.html

The kernel knows the mapping between these id's and the underlying resources.

```c
x = connect(/* connection details */); // -> 5
x.write("string"); // turns into write(5, "string")
```

The syscall for read is generally `read(id) -> data`. The issue here is that the calls to read
happen synchronously in the following code:

```c
x.read();
y.read();
z.read();
```

For reads to happen simultaneously we need async io, which the kernel allows you to do
by passing `flags` to read: `read(id, flags) -> data`. One of these flags is `O_NONBLOCK`.
It is not set on the read, but set on the socket, which makes the read operation say
there was no data to read. Normally the kernel just stops the thread until there is more
data.

Suppose the connection with id 4 has no data to read currently. Calling read would cause
an error `WOULD_BLOCK`

```c
read(4); // -> error WOULD_BLOCK
```

If we set the `O_NONBLOCK` flag on all the connections `x, y, z`, calling read on them
is like polling them in rust futures.

Suppose a future calls `read` and receives the `WOULD_BLOCK` error, it could now
register its handler on the other thread and return `NotReady`. Having the other thread
loop over every handler and check if they have incoming data would be inefficient.

A better interface for checking if a resource is available is `epoll`. `epoll` takes in
a bunch of file descriptors and a bunch of testing operations which tell you if you
can read/write/do other io.

https://man7.org/linux/man-pages/man7/epoll.7.html

```c
epoll((fd, op)...); // -> fd... or (fd, op)...
```

The idea is that a thread can call epoll which tells the kernel to block the thread
until an operation is ready for it's file descriptor. The kernel internally adds
markers to each of the resources so that when the resource is available, it's going
to unblock the call similar to notify.

epoll when called is first going to check all of the resources and immediately return
if the operation is available.

The other thread can be given a task, resource id a.k.a file descriptor and an operation to
initialize the future. Once epoll returns, it can tell the other thread the file descriptors
which are now ready for reading/writing etc.

Inside the other thread that wakes things up:

```rust
// What the resource can now do: can read/can write
enum Operation {
  Read,
  Write
}

fn reactor_thread(notify_me: mpsc::Receiver<(Task, FD, Operation)>) {
  let waiting_for = HashMap<(FD, Operation), Task>;

  loop {
    // accepts new things to watch for (using epoll)
    while let Some((task, fd, op)) = notify_me.try_recv() {
      waiting_for.insert((fd, op), task);
    }

    let select = waiting_for.keys().collect();
    for (fd, op) in epoll(select) {
      // when the resource is available, notify
      let task = waiting_for.remove((fd, op)).notify();
    }
  }
}
```

Why does this even need to be a separate thread? It doesn't!
We have time in the executor where we are waiting for futures to be notified
so let's use that for the reactor:

```rust
// ...
impl Executor {
  fn run_all<F>(&mut self, futures: Vec<F>) -> Vec<(usize, Result<F::Item, F::Error>)>
    where F: Future
  {
    let mut done = 0;
    let mut results = Vec::with_capacity(futures.len());
    let nf = Arc::new(MyNotifier(Mutex::new(vec![true; futures.len()])));
    let notifier = NotifyHandle::from(nf);
    let waiting_for = HashMap<(FD, Operation), Task>;

    while done != futures.len() {
      for (i, f) in futures.iter_mut().enumerate() {
        let was_notified = self.0.lock();
        if !was_notified[i] {
          continue;
        }
        was_notified[i] = false;
        drop(was_notified); // prevents a dead-lock if poll tries to notify immediately

        match tokio::reactor::with_default(&waiting_for, || executor::with_notify(
          &notifier, i, || f.poll()
        )) {
          // tokio::reactor::Handle::current() -> Handle
          Ok(Async::Ready(t)) => {
            results.push(i, Ok(t));
            done += 1;
          }
          Err(e) => {
            results.push(i, Err(e));
            done += 1;
          }
          Ok(Async::NotReady) => {
            continue;
          }
        }
      }

      let select = waiting_for.keys().collect();
      // it's fine to block until we are ready again and no time is wasted
      for (fd, op) in epoll(select) {
        let task = waiting_for.remove((fd, op)).notify();
      }
    }

    results
  }
}
```

Because this is now single-threaded, we don't need the arc and mutex.

The next problem is how does `poll` add values to `waiting_for` or how does `poll`
say that it wants to wait on a file descriptor.

`tokio::reactor` has a `with_default` function which sets the current reactor.
`Handle::current` is used to get the current reactor from inside the future's poll.

The task can be used to notify and the handle to add an io resource to waiting_for

Finally we can implement a future Foo with an implementation of poll

```rust
struct Foo {
  // But with O_NONBLOCK set
  fd: std::net::TcpStream,
}

impl Future for Foo {
  type Item = ();
  type Error = ();

  fn poll(&mut self) -> Result<Async<Self::Item>, Self::Error> {
    match self.fd.read() {
      Err(io::Error::WouldBlock) => {
        // we also need to do something to make sure we are woken up
        // as the contract says we must
        let reactor = Handle::current();
        // note: not in tokio
        reactor.register(self.fd, Operation::Read, task::current());
        Ok(Async::NotReady)
      }
      Err(io::Error::Closed) => {
        Ok(Async::Ready(()))
      }
      Err(e) => Err(e)
      Ok(r) => {
        eprintln!("Got {} bytes", r.len())
        // how do we collect the data?!
        // we could go to sleep again and wait for the epoll to notify
        let reactor = Handle::current();
        // note: not in tokio
        reactor.register(self.fd, Operation::Read, task::current());
        Ok(Async::NotReady)
      }
    }
  }
}
```

Instead of putting the future to sleep we could do the operation in a loop
because usually data comes in chunks:

```rust
  // ...
  fn poll(&mut self) -> Result<Async<Self::Item>, Self::Error> {
    loop {
      match self.fd.read() {
        Ok(r) => {
          eprintln!("Got {} bytes", r.len())
        }
        Err(io::Error::WouldBlock) => {
          let reactor = Handle::current();
          match PollEvented::new_with_handle(self.fd, reactor).poll_read_ready() {
            Ok(Async::Read(_)) => {
              // socket became ready between when we read and called poll_read_ready
              continue;
            }
            Ok(Async::NotReady) => return Ok(Async::NotReady),
            Err(e) => return Err(e),
          }
        }
        Err(io::Error::Closed) => {
          Ok(Async::Ready(()))
        }
        Err(e) => Err(e)
      }
    }
  }
```

`PollEvented` is a struct which wraps the file descriptor

## Driving futures with Tokio

`tokio::runtime` provides a reactor, an executor and a _timer_.

https://docs.rs/tokio/0.1.22/tokio/runtime/index.html

A timer could just be a thread checking the time over and over again, but
we can again use the executor's unused time (like with the reactor) to check
if any timers have expired.

Now instead of epoll we use a variant with a timeout to make sure we don't miss
the timers.

Timer is very similar to reactor in many ways

> If you have tons of futures, you need to find a way to drive the reactor and driver
> and not spend all time polling. In tokio a timer has a granularity of 1ms

Spawning a future is useful for listeners:

```rust
let server = TcpListener::new("127.0.0.1:1234")
  .incoming() // a stream is like an iterator but asynchronous
  .for_each(|s: TcpStream| {
    // tokio::spawn(fut) <=> tokio::runtime::Handle::current().spawn(fut)
    tokio::spawn(Client::new(s))
  })
```

Where a `Client` is a future which outputs the incoming data to files for example

Pseudo-rust of an executor with spawn

```rust
struct Executor<F: Future>(Arc<Mutex<Vec<bool>>>, Vec<F>)

impl Executor {
  fn spawn<F>(&mut self, fut: F)
    where F: Future {
    self.1.push(fut);
  }
}
```

Another method that `tokio::runtime` has is `block_on` which basically
blocks until the future resolves or throws an error. It's like the `run_all`
but it returns the result only for the given future

```rust
pub fn block_on<F>(&mut self, future: F) -> Result<F::Item, F::Error> where
  F: Future
```

> Note that the executor will exit with the results once all futures have resolved

The `run` method is `run_all` but it doesn't capture the output results

`tokio::run` is going to use the default runtime. The way the default runtime
is special is that it uses a thread pool.

https://docs.rs/tokio-threadpool/0.1.18/tokio_threadpool/index.html

## What is async/await?

```rust
let buf = String::from("foobar");
let fut = TcpStream::connect("127.0.0.1")
  .and_then(|c| c.write(buf))
  .and_then(|c| c.read())
  .and_then(|(c, b)| b == "barfoo");

println!("{:?}", fut);
let x = tokio::run(fut);
println!("{}", buf);
```

Here the lifetime of buf is bound to the lifetime and as the future is consumed by tokio::run
buf dies before the last println

Async blocks:

```rust
let x = async {
  let c = await! { TcpStream::connect("127.0.0.1") };
  await! { c.write("foobar") };
  let b = await! { c.read() };
  b == "barfoo"
}
```

Async await code is linear and easier to read

The old style:

```rust
fn check_foobar() -> impl Future {
  // and_then chaining
}
fn check_foobar() -> FoobarCheckFuture {
  enum FooBarCheckFuture {
    Connecting(TcpStreamConnectFuture),
    Writing(WriteFuture),
    Reading(ReadFuture),
  }

  impl Future for FooBarCheckFuture {

  }
}
```

Versus the new async/await style

```rust
fn async check_foobar() -> impl Future {
  let c = await! { TcpStream::connect("127.0.0.1") };
  await! { c.write("foobar") };
  let b = await! { c.read() };
  b == "barfoo"
}
```

The async keyword constructs a type that is a future and it will run from the
previous await point to the next every time it can make progress.

When you call poll on the constructed future it start running from the start of the block
until it hits the first await. The first await is given a future as an argument
`TcpStream::connect("127.0.0.1")` for example, and it's going to poll that future.
If that future is Ready then it keeps executing until the next await.

But if the awaited future is not ready, async will keep track of where the previous
poll got to.

https://rust-lang.github.io/rfcs/2394-async_await.html#the-expansion-of-await

```rust
fn async check_foobar() -> impl Future {
  let c = await! { TcpStream::connect("127.0.0.1") };
  await! { c.write("foobar") };
  // let b = await! { c.read() };
  let b = loop {
    match c.read() {
      Async::Ready(x) => break x,
      Async::NotReady => {
        // if we get to this point, you can think of yield as
        // return Async::NotReady, except the next poll would re-execute the connect
        yield // remembers where the previous poll returned from
      },
    }
  };

  b == "barfoo"
}
```

The basic idea of async await is that we need to continue from where we stopped.

The problem is when we continue, where does `c` come from?

The code async generates is similar to a generator, but is asynchronous and needs
to deal with wakeups

The compiler generates an unique enum for every async block, and for every await
it creates a new "step" in the enum which store all of the state so far.

Every time you poll the generated future, you're really polling the current step's
waiting_on

```rust
enum CompilerMadeAsyncBlock /* with a unique id */ {
  Step0(Vec<bool>), // state before first poll (what the async block captures)
  Step1 { // await! { TcpStream::connect("127.0.0.1") }
    z: Vec<bool>,
    waiting_on: impl Future<Output = TcpStream>,
  },
  Step2 {
    z: Vec<bool>,
    c: TcpStream,
    // future returned from c.write()
    // note that c.write takes a mutable reference to self and is therefore
    // tied to the lifetime of c
    waiting_on: impl Future<Output = usize> + 'c,
  }
}
impl Future for CompilerMadeAsyncBlock {

}

let bar = vec![true];
let fut = async -> impl Future /* actually CompilerMadeAsyncBlock_123... */ {
  let z = bar;
  let c = await! { TcpStream::connect("127.0.0.1") };
  await! { c.write("foobar") };
  // let b = await! { c.read() };
  let b = loop {
    match c.read() {
      Async::Ready(x) => break x,
      Async::NotReady => {
        // if we get to this point, you can think of yield as
        // return Async::NotReady, except the next poll would re-execute the connect
        yield // remembers where the previous poll returned from
      },
    }
  };

  b == "barfoo"
}
```

## Self-referential data-types

Notice that `Step2` is a self-referential datatype because waiting_on depends on c

Example of a self-referential datatype

```rust
struct Foo {
  data: [u8; 1024],
  half: &[u8], // into self.data
}

let foo = Foo {
  data: [0; 1024]
  half: // ??? but we don't have a reference to foo yet
}

// suppose we managed to create foo
let z = Box::new(foo);
z.data[0] = 1;
// z.half is still pointing to foo on the stack and is not tied to z.data
```

The problem is that async/await needs self-referential behavior because the future
waiting_on at Step2 is using the stream.

There have been many attempts to add self-referential datatypes to rust which is really hard.

One way could be to have a raw pointer to the data

```rust
struct Foo {
  data: [u8; 1024],
  // half: &[u8],
  half: *const u8, // manage ourselves
}
```

But that's not ideal if the programmer is allowed to move foo around because there's no
way to guarantee the pointer is correct.

Can we guarantee somehow that foo does not move once we initialize `data` and `half`.

This notion of a self-referential datatype has boiled down to the notion of something
that doesn't move.

There was an attempt to add a trait for it

```rust
impl !Move for Foo {}
```

## Pinning

Useful blogs https://without.boats/blog/

> Anything that talks about pinning is great for understanding it

https://rust-lang.github.io/rfcs/2349-pin.html

Pinning introduces 2 new types

```rust
struct Pin<P>;
// P is a pointer type

/// If T: Unpin, it is not sensitive to being moved
trait Unpin {}
// a marker trait which is defined automatically for all types
// (that can unpin) by the compiler
// > Unpin is not an empty trait

impl !Unpin for MyType {}
// Unless you have an impl like this, every type you make auto-implements Unpin
// This means that if you have a pin to MyType, you cannot unpin it

fn bar<T>(x: Pin<&mut T>) {
  // Promising:
  // - either T will never move again
  // - or T: Unpin
}
```

```rust
// an example of a struct Bar: Unpin
struct Bar {
  v: Vec<bool>,
  buf: Vec<u8>,
  z: Arc<Mutex<HashMap<(usize, bool), String>>>
}
```

In the context of `Foo: !Unpin` only once you know you are going to access `half`
it is sensitive to moving. That is, you can move foo's around as long as you are
not using `half`.

It's only because of the semantics of `Foo.half` that makes `Foo` sensitive. The
compiler doesn't know that `*const u8` is a pointer to self.

If you set `half` but never access it, `Foo` stays Unpin because nothing breaks
even if foo is moved in memory.

Implementing Deref for Pin is straight-forward

```rust
struct Pin<P>(ptr: P);

impl<P, T> Deref for Pin<P> where P::Target: T {
  type Target = P::Deref;
  fn deref(&self) -> &Self::Target {
    &*self.ptr
  }
}
```

But DerefMut is problematic because it gives a mutable pointer to P
which can be abused

```rust
impl<P, T> DerefMut for Pin<P>
where P::Target: T {
  fn deref_mut(&mut self) -> &mut Self::Target {
    &mut *self.ptr
  }
}

// Foo: !Unpin
let f: Pin<Box<Foo>>;

// &mut *f gives a mutable pointer to foo
// the following replace is safe but the pin's
// contract is broken, f moves to z
let z = mem::replace(&mut *f, anotherfoo);
```

The solution is to restrict DerefMut

```rust
impl<P, T> DerefMut for Pin<P>
// we have to make sure T is Unpin
where P::Target: T, T: Unpin {
  fn deref_mut(&mut self) -> &mut Self::Target {
    &mut *self.ptr
  }
}

// Foo: !Unpin
let f: Pin<Box<Foo>>;

// &mut *f gives a mutable pointer to foo
// the following replace is safe but the pin's
// contract is broken, f moves to z
let z = mem::replace(&mut *f, anotherfoo); // compiler error: Foo does not impl Unpin
```

If we have code which is not moving foo, but changes just the `data` instead
getting a mutable reference to foo has to be unsafe. We can use this function
if we promise we won't move foo

```rust
impl<P, T> Pin<P> {
  unsafe fn unsafe_as_mut(&mut self) -> &mut P::Target where P: DerefMut {
    &mut *self.ptr
  }
}
```

Notice that DerefMut extends Deref

```rust
trait DerefMut: Deref {}
```

Pin is only one level deep

```rust
struct Bar {
  v: Vec<bool>,
  buf: Vec<u8>,
  z: Arc<Mutex<HashMap<(usize, bool), String>>>,
  foo: Box<Foo>,
}
```

Notice that it is totally fine to move Foo inside Bar

```rust
// All that this Pin promises is that bar is not going to move
let bar: Pin<Box<Bar>>;
```

Pins also have a as_mut function which converts the reference to a mutable reference
which is safe

```rust
impl<P, T> Pin<P> {
  unsafe fn unsafe_as_mut(&mut self) -> &mut P::Target where P: DerefMut {
    &mut *self.ptr
  }

  // Pin<Box<Foo>> -> Pin<&mut Foo>
  fn as_mut(&mut self) -> Pin<&mut P::Target> where P: DerefMut {
    Pin(&mut *self.ptr)
  }
}
```

Going back to futures: calling `poll` (which takes in `Pin<&mut self>`) on a
`Pin<Box<FooFuture>>` is possible using `as_mut`

```rust
let f: Pin<Box<FooFuture>>;
f.as_mut().poll();
```

## Pinning in async/await

```rust
enum CompilerMadeAsyncBlock /* with a unique id */ {
  Step0(Vec<bool>), // state before first poll (what the async block captures)
  Step1 { // await! { TcpStream::connect("127.0.0.1") }
    z: Vec<bool>,
    waiting_on: impl Future<Output = TcpStream>,
  },
  Step2 {
    z: Vec<bool>,
    c: TcpStream,
    // future returned from c.write()
    // note that c.write takes a mutable reference to self and is therefore
    // tied to the lifetime of c
    waiting_on: impl Future<Output = usize> + 'c,
  }
}
impl !Unpin for CompilerMadeAsyncBlock {}
impl Future for CompilerMadeAsyncBlock {

}
```

`CompilerMadeAsyncBlock` is `!Unpin` because the future `waiting_on` in `Step2` references
`Step2.c`. But once we start polling `CompilerMadeAsyncBlock` we know it's not going to
move and before we start polling it's fine to be moved. It's only at the first time you
call poll, when it's going to add a pin to it.

Now the full expansion

```rust
let mut future = IntoFuture::into_future($expression);
let mut pin = unsafe { Pin::new_unchecked(&mut future) };
loop {
  match Future::poll(Pin::borrow(&mut pin), &mut ctx) {
    Poll::Ready(item) => break item,
    Poll::Pending     => yield,
  }
}
```

The second line pins the mutable reference to the future, establishing the contract
that from this point on, the future must not move.

Next the loop starts polling the pin of the mutable reference to the future.

In the generated enum from the async blocks, all the steps are calls to poll which
means that we are getting a pin of mut self making the self referenctial parts fine.

Until you call await!/start polling it, you are free to move it because you can think
of the async block's future being in the state Step0 which doesn't have references to
self.

https://doc.rust-lang.org/nightly/unstable-book/language-features/optin-builtin-traits.html

```rust
// nightly feature: opt out of auto traits
impl !Send for MyTYpe {}
```

https://docs.rs/pin-utils/0.1.0/pin_utils/

```rust
struct Bar {
  v: Vec<bool>,
  buf: Vec<u8>,
  z: Arc<Mutex<HashMap<(usize, bool), String>>>,
  foo: Box<Foo>,
}

let bar: Pin<Box<Bar>>;
// is Pin<&mut Bar> -> Pin<&mut Foo> using bar.foo safe?

// this is safe
impl<T> Pin<P> where P: Deref, P::Target: Unpin {
  fn new(pointer: P) -> Self {
    Pin(pointer)
  }
}

// this is unsafe because we want to avoid anyone having
// a mutable reference to the contained value, and a
// reference type with a malicious deref_mut may break
// the pin contract
impl<T> Pin<P> where P: Deref {
  // using this function you promise that the reference
  // types implement DerefMut without mutating the value
  unsafe fn new_unchecked(pointer: P) -> Self {}
}

struct BadBox<T>(Box<T>);

impl Deref for BadBox {...}

impl<T> DerefMut for BadBox<T> {
  fn deref_mut(&mut self) -> &mut Self::Target {
    // here badbox has a mutable reference to the contained value
    let old_t = mem::replace(&mut *self.0, mem::uninitialized());
    &mut *self.0
  }
}
```

⬜
