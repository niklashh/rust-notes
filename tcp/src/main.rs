use std::io;
use std::io::prelude::*;
use std::thread;

fn main() -> io::Result<()> {
    let mut i = tcp::Interface::new()?;
    let mut l1 = i.bind(9000)?;

    let jh1 = thread::spawn(move || {
        while let Ok(ref mut stream) = l1.accept() {
            eprintln!("got connection");

            stream.write(b"hello").unwrap();
            stream.shutdown(std::net::Shutdown::Write).unwrap();
            loop {
                let mut buf = [0; 512];
                let n = stream.read(&mut buf).unwrap();
                eprintln!("read {} bytes of data", n);
                if n == 0 {
                    eprintln!("no more data");
                    break;
                } else {
                    println!("data: {}", std::str::from_utf8(&buf[..n]).unwrap());
                }
            }
        }
    });

    jh1.join().unwrap();

    Ok(())
}
