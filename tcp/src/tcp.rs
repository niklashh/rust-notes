use std::collections::{BTreeMap, VecDeque};
use std::{io, time};

bitflags::bitflags! {
    pub(crate) struct Available: u8 {
        const READ = 0b01;
        const WRITE = 0b10;
    }
}

#[derive(Debug)]
enum State {
    SynRcvd,  // client has sent syn, and we have sent syn+ack
    Estab,    // three-way handshake has finished
    FinWait1, // we have sent a fin but it hasn't been acked
    FinWait2, // our fin has been acked
    TimeWait, // connection has closed
}

impl State {
    fn is_synchronized(&self) -> bool {
        match *self {
            State::SynRcvd => false,
            State::Estab | State::FinWait1 | State::FinWait2 | State::TimeWait => true,
        }
    }
    fn have_sent_fin(&self) -> bool {
        match *self {
            State::SynRcvd | State::Estab => false,
            State::FinWait1 | State::FinWait2 | State::TimeWait => true,
        }
    }
}

pub struct Connection {
    state: State,
    send: SendSequenceSpace,
    recv: RecvSequenceSpace,
    ip: etherparse::Ipv4Header,
    tcp: etherparse::TcpHeader,
    timers: Timers,

    pub(crate) incoming: VecDeque<u8>,
    pub(crate) unacked: VecDeque<u8>,
    pub(crate) closed: bool,
    closed_at: Option<u32>,
}

struct Timers {
    send_times: BTreeMap<u32, time::Instant>,
    srtt: f64,
}

/// RFC 793 S3.2 F4
struct SendSequenceSpace {
    /// send unacknowledged
    una: u32,
    /// send next
    nxt: u32,
    /// send window
    wnd: u16,
    /// send urgent pointer
    up: bool,
    /// segment sequence number used for last window update
    wl1: usize,
    /// segment acknowledgment number used for last window update
    wl2: usize,
    /// initial send sequence number
    iss: u32,
}

/// RFC 793 S3.2 F5
struct RecvSequenceSpace {
    /// receive next
    nxt: u32,
    /// receive window
    wnd: u16,
    /// receive urgent pointer
    up: bool,
    /// initial receive sequence number
    irs: u32,
}

impl Connection {
    pub(crate) fn is_rcv_closed(&self) -> bool {
        if let State::TimeWait = self.state {
            true
        } else {
            false
        }
    }

    fn availability(&self) -> Available {
        let mut a = Available::empty();
        if self.is_rcv_closed() || !self.incoming.is_empty() {
            a |= Available::READ;
        }
        a
    }

    // 'a is the lifetime of the packet buffer
    pub fn accept<'a>(
        nic: &mut tun_tap::Iface,
        iph: etherparse::Ipv4HeaderSlice<'a>,
        tcph: etherparse::TcpHeaderSlice<'a>,
        data: &'a [u8],
    ) -> io::Result<Option<Self>> {
        // we are expecting to get a packet with SYN set
        if !tcph.syn() {
            // not SYN
            return Ok(None);
        }

        let iss = 0;
        let wnd = 10;
        let mut c = Connection {
            closed: false,
            closed_at: None,
            timers: Timers {
                send_times: Default::default(),
                srtt: time::Duration::from_secs(60).as_secs_f64(),
            },
            state: State::SynRcvd,
            send: SendSequenceSpace {
                iss,
                una: iss,
                nxt: iss,
                wnd: wnd,
                up: false,
                wl1: 0,
                wl2: 0,
            },
            recv: RecvSequenceSpace {
                irs: tcph.sequence_number(),
                nxt: tcph.sequence_number().wrapping_add(1),
                wnd: tcph.window_size(),
                up: false,
            },
            tcp: etherparse::TcpHeader::new(tcph.destination_port(), tcph.source_port(), iss, wnd),
            ip: etherparse::Ipv4Header::new(
                0,
                64,
                etherparse::IpTrafficClass::Tcp,
                [
                    iph.destination()[0],
                    iph.destination()[1],
                    iph.destination()[2],
                    iph.destination()[3],
                ],
                [
                    iph.source()[0],
                    iph.source()[1],
                    iph.source()[2],
                    iph.source()[3],
                ],
            ),
            incoming: Default::default(),
            unacked: Default::default(),
        };

        c.tcp.syn = true;
        c.tcp.ack = true;
        c.write(nic, c.send.nxt, 0)?;

        Ok(Some(c))
    }

    fn write(&mut self, nic: &mut tun_tap::Iface, seq: u32, mut limit: usize) -> io::Result<usize> {
        let mut buf = [0u8; 1500];
        self.tcp.sequence_number = seq;
        self.tcp.acknowledgment_number = self.recv.nxt;

        dbg!(&"-----------WRITE------------", seq, limit, &self.tcp);

        let mut offset = seq.wrapping_sub(self.send.una) as usize;
        // we need to have a special case for SYN and FIN
        if let Some(closed_at) = self.closed_at {
            if seq == closed_at.wrapping_add(1) {
                offset = 0; // we should not be sending any data
                limit = 0;
            }
        }
        let (mut h, mut t) = self.unacked.as_slices();
        dbg!(h, t);
        if h.len() >= offset {
            h = &h[offset..];
        } else {
            let skipped = h.len();
            h = &[];
            t = &t[(dbg!(offset) - dbg!(skipped))..];
        }

        let max_data = std::cmp::min(limit, h.len() + t.len());
        let size = std::cmp::min(
            buf.len(),
            self.tcp.header_len() as usize + self.ip.header_len() as usize + max_data,
        );
        self.ip
            .set_payload_len(size - self.ip.header_len() as usize)
            .unwrap(); // ignoring etherparse::ValueError

        self.tcp.checksum = self
            .tcp
            .calc_checksum_ipv4(&self.ip, &[])
            .expect("failed to compute checksum");

        use std::io::Write;
        let mut unwritten = &mut buf[..];
        self.ip.write(&mut unwritten); // ignoring etherparse::WriteError
        self.tcp.write(&mut unwritten)?;
        let payload_bytes = {
            let mut written = 0;
            let mut limit = max_data;

            let p1l = std::cmp::min(limit, h.len());
            written += unwritten.write(&h[..p1l])?;
            limit -= written;

            let p2l = std::cmp::min(limit, t.len());
            written += unwritten.write(&t[..p2l])?;
            written
        };
        let unwritten = unwritten.len();

        let next_seq = seq.wrapping_add(
            payload_bytes as u32
                + if self.tcp.syn { 1 } else { 0 }
                + if self.tcp.fin { 1 } else { 0 },
        );

        // reset syn and fin
        self.tcp.syn = false;
        self.tcp.fin = false;

        // don't update snd.nxt in retransmissions
        if wrapping_lt(self.send.nxt, next_seq) {
            self.send.nxt = next_seq;
        }
        self.timers.send_times.insert(seq, time::Instant::now());

        nic.send(&buf[..buf.len() - unwritten])?;
        Ok(payload_bytes)
    }

    fn send_rst(&mut self, nic: &mut tun_tap::Iface) -> io::Result<()> {
        self.tcp.rst = true;
        // TODO fix sequence numbers
        // TODO handle sync RST
        self.tcp.sequence_number = 0;
        self.tcp.acknowledgment_number = 0;
        self.write(nic, self.send.nxt, 0)?;
        Ok(())
    }

    pub(crate) fn on_tick<'a>(&mut self, nic: &mut tun_tap::Iface) -> io::Result<()> {
        let nunacked = self.send.nxt.wrapping_sub(self.send.una);
        let unsent = self.unacked.len() as u32 - nunacked;

        // how long has it been since we sent the last thing that is not unacked
        let waited_for = self
            .timers
            .send_times
            .range(self.send.una..)
            .next()
            .map(|t| t.1.elapsed());

        let should_retransmit = if let Some(waited_for) = waited_for {
            waited_for > time::Duration::from_secs(1)
                && waited_for.as_secs_f64() > 1.5 * self.timers.srtt
        } else {
            false
        };

        if should_retransmit {
            // TODO retransmit
            let resend = std::cmp::min(self.unacked.len() as u32, self.send.wnd as u32);
            if self.unacked.len() == 0 && resend < self.send.wnd as u32 && self.closed {
                // can we include the FIN?
                self.tcp.fin = true;
                self.closed_at = Some(self.send.una.wrapping_add(self.unacked.len() as u32));
                // TODO self.closed_at
            }
            self.write(nic, self.send.una, resend as usize)?;
        } else {
            // TODO send new data
            if unsent == 0 && self.closed_at.is_some() {
                return Ok(());
            }

            let allowed = self.send.wnd as u32 - nunacked;
            if allowed == 0 {
                return Ok(());
            }

            let send = std::cmp::min(unsent, allowed);
            if unsent == 0 && send < allowed && self.closed && self.closed_at.is_none() {
                self.tcp.fin = true;
                self.closed_at = Some(self.send.una.wrapping_add(self.unacked.len() as u32));
            }

            self.write(nic, self.send.nxt, send as usize)?;
        };

        Ok(())
    }

    pub(crate) fn on_packet<'a>(
        &mut self,
        nic: &mut tun_tap::Iface,
        iph: etherparse::Ipv4HeaderSlice<'a>,
        tcph: etherparse::TcpHeaderSlice<'a>,
        data: &'a [u8],
    ) -> io::Result<Available> {
        let seqn = tcph.sequence_number();
        let seglen = data.len() as u32 + tcph.fin() as u32 + tcph.syn() as u32;
        let wend = self.recv.nxt.wrapping_add(self.recv.wnd as u32);
        let okay = if data.len() == 0 && !tcph.syn() && !tcph.fin() {
            // zero-length segment has separate rules for acceptance
            if self.recv.wnd == 0 && seqn != self.recv.nxt {
                false
            } else if !is_between_wrapped(self.recv.nxt.wrapping_sub(1), seqn, wend) {
                false
            } else {
                true
            }
        } else {
            if self.recv.wnd == 0 {
                false
            } else if
            // valid segment size check
            // okay if it acks at least one byte, which means at least on of the following is true
            // RCV.NXT =< SEG.SEQ               < RCV.NXT + RCV.WND (checks the first byte of the segment)
            // RCV.nxt =< SEG.SEQ + SEG.LEN - 1 < RCV.NXT + RCV.WND (checks the last byte of the segment)
            !is_between_wrapped(self.recv.nxt.wrapping_sub(1), seqn, wend)
                && !is_between_wrapped(
                    self.recv.nxt.wrapping_sub(1),
                    seqn.wrapping_add(seglen - 1),
                    wend,
                )
            {
                false
            } else {
                true
            }
        };

        if !okay {
            self.write(nic, self.send.nxt, 0)?;
            return Ok(self.availability());
        }

        if !tcph.ack() {
            if tcph.syn() {
                // got SYN part of initial handshake
                assert!(data.is_empty());
                self.recv.nxt = seqn.wrapping_add(1);
            }
            // drop the segment and return
            return Ok(self.availability());
        }

        // acceptable ack check RFC 793 S3.3
        // SND.UNA < SEG.ACK =< SND.NXT
        let ackn = tcph.acknowledgment_number();

        if let State::SynRcvd = self.state {
            if is_between_wrapped(
                self.send.una.wrapping_sub(1),
                ackn,
                self.send.nxt.wrapping_add(1),
            ) {
                self.state = State::Estab;
            } else {
                unimplemented!();
                // TODO <SEQ=SEG.ACK><CTL=RST>
            }
        }

        if let State::Estab | State::FinWait1 | State::FinWait2 = self.state {
            if is_between_wrapped(self.send.una, ackn, self.send.nxt.wrapping_add(1)) {
                if !self.unacked.is_empty() {
                    let nacked = self
                        .unacked
                        .drain(..ackn.wrapping_sub(self.send.una) as usize)
                        .count();

                    let old = std::mem::replace(&mut self.timers.send_times, BTreeMap::new());
                    let una = self.send.una;
                    let srtt = &mut self.timers.srtt;
                    self.timers
                        .send_times
                        .extend(old.into_iter().filter_map(|(seq, sent)| {
                            if is_between_wrapped(una, seq, ackn) {
                                // RFC793 S3.7
                                // SRTT = ( ALPHA * SRTT ) + ((1-ALPHA) * RTT)
                                *srtt = 0.8 * *srtt + (1.0 - 0.8) * sent.elapsed().as_secs_f64();
                                None
                            } else {
                                Some((seq, sent))
                            }
                        }));
                }
                self.send.una = ackn;
            }

            // TODO prune self.unacked
            // TODO if unacked empty and waiting flush, notify
            // TODO update window
        }

        if let State::FinWait1 = self.state {
            if self.send.una == self.send.iss + 2 {
                // our FIN has been acked
                self.state = State::FinWait2;
            }
        }

        if let State::Estab | State::FinWait1 | State::FinWait2 = self.state {
            // TODO only read stuff we haven't read
            let mut unread_data_at = (self.recv.nxt - seqn) as usize;

            if unread_data_at > data.len() {
                // we must hace received a re-transmitted FIN that we have already counted
                // recv.nxt points beyond the fin, but the fin is not part of data
                assert_eq!(unread_data_at, data.len() + 1);
                unread_data_at = 0;
            }

            self.incoming.extend(&data[unread_data_at..]);

            dbg!(&self.incoming);

            // TODO wake up waiting readers

            self.recv.nxt = seqn
                .wrapping_add(data.len() as u32)
                .wrapping_add(if tcph.fin() { 1 } else { 0 });

            // Send an acknowledgment <SEQ=SND.NXT><ACK=RCV.NXT><CTL=ACK>
            self.write(nic, self.send.nxt, 0)?;
        }

        if tcph.fin() {
            match self.state {
                State::FinWait2 => {
                    // we're done with the connection
                    self.write(nic, self.send.nxt, 0)?;
                    self.state = State::TimeWait
                }
                _ => unimplemented!(),
            }
        }

        Ok(self.availability())
    }

    pub(crate) fn close(&mut self) -> io::Result<()> {
        self.closed = true;
        match self.state {
            State::SynRcvd | State::Estab => {
                self.state = State::FinWait1;
            }
            State::FinWait1 | State::FinWait2 => {}
            _ => {
                return Err(io::Error::new(
                    io::ErrorKind::NotConnected,
                    "already closing",
                ))
            }
        }
        Ok(())
    }
}

/// RFC1323 S2.3
/// TCP determines if a data segment is "old" or "new" by testing
/// whether its sequence number is within 2**31 bytes of the left edge
/// of the window, and if it is not, discarding the data as "old".  To
/// insure that new data is never mistakenly considered old and vice-
/// versa, the left edge of the sender's window has to be at most
/// 2**31 away from the right edge of the receiver's window.
fn wrapping_lt(lhs: u32, rhs: u32) -> bool {
    lhs.wrapping_sub(rhs) > 2 ^ 31
}

fn is_between_wrapped(start: u32, x: u32, end: u32) -> bool {
    wrapping_lt(start, x) && wrapping_lt(x, end)
}

#[allow(dead_code)]
fn is_between_wrapped_old(start: u32, x: u32, end: u32) -> bool {
    // |-S--X--E-|-S--X--E-|
    //           ^-- imagine this break wasn't there
    //
    // from the diagram we see that all the following orderings are valid
    // S < X < E
    // X < E < S
    // E < S < X

    if start < x && x < end || x < end && end < start || end < start && start < x {
        true
    } else {
        false
    }
}

#[cfg(test)]
mod is_between_wrapped {
    use super::*;

    #[test]
    fn all_valid_cases() {
        assert_eq!(is_between_wrapped(0, 1, 2), true);
        assert_eq!(is_between_wrapped(u32::MAX, 0, 1), true);
        assert_eq!(is_between_wrapped(u32::MAX - 1, u32::MAX, 1), true);
    }

    #[test]
    fn no_wrapping_invalid() {
        assert_eq!(is_between_wrapped(2, 4, 3), false);
        assert_eq!(is_between_wrapped(2, 1, 3), false);
    }

    #[test]
    fn wrapping_invalid() {
        assert_eq!(is_between_wrapped(3, 2, 1), false);
    }

    #[test]
    fn start_is_end() {
        assert_eq!(is_between_wrapped(1, 1, 1), false);
        assert_eq!(is_between_wrapped(1, 0, 1), false);
        assert_eq!(is_between_wrapped(1, 2, 1), false);
    }
}
