#!/bin/sh -em
cargo b --release
sudo setcap CAP_NET_ADMIN=eip target/release/tcp
target/release/tcp &
sudo ip a a 10.10.10.1/24 dev tun0
sudo ip l s up dev tun0
fg
