# Implementing TCP in Rust

Video url: https://www.youtube.com/watch?v=bzja9fQWzdA

TCP is defined in RFC 793. A TCP tutorial can be found at https://tools.ietf.org/html/rfc1180

## Core requirements

https://tools.ietf.org/html/rfc7414

1. RFC 793
   The fundamental TCP specification
1. RFC 1122
   Some additional extensions
1. RFC 5681
   Basic congestion control, slow start

What we don't need

1. RFC 2460
   IPv6 support
1. RFC 2873
   Removes processing of some bits

## Implementing TCP in Linux

`libpnet` is a low level API for capturing raw packets. The only issue is that the kernel will still
be running it's own TCP stack on the network interface which might interfere.

Instead we will use TUN/TAP interfaces, which are a userspace system to create network interfaces in
the kernel. Documentation: https://www.mjmwired.net/kernel/Documentation/networking/tuntap.txt

There's a crate for tunnels and taps: https://docs.rs/tun-tap/0.1.2/tun_tap/

Note that we have to implement the IP protocol as well, which is easy relative to TCP

IP protocol is defined in RFC 791: https://tools.ietf.org/html/rfc791

The header format for an IP packet is the following

```
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |Version|  IHL  |Type of Service|          Total Length         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |         Identification        |Flags|      Fragment Offset    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Time to Live |    Protocol   |         Header Checksum       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                       Source Address                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Destination Address                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Options                    |    Padding    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```

Generally you have to be root to do networking stuff, but we give a kernel capability to our process
called `CAP_NET_ADMIN` to give it networking permissions

Networking uses big endianness

The different types of payloads an ethernet frame can carry are listed here:
https://en.wikipedia.org/wiki/EtherType

0x0800 is IPv4

0x86DD is IPv6

The payload that follows the ethertype is the payload, the IP packet

We can parse the IP packet using a library https://docs.rs/etherparse/0.9.0/etherparse/index.html

IP protocol numbers https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers

## Sequence numbers

In TCP every byte of data has a sequence number

When the client responds with ACK 8, it's saying that it has acknowledged all bytes until
(non-inclusive) the byte with sequence number 8

The sequence number space is 32 bits long, and they wrap around when you send enough data

If SYN is present, the sequence number is the
initial sequence number (ISN) and the first data octet is ISN+1

The acknowledgement number is the previous sequence number +1

## TCB

The transmission control block is the state kept for every connection

https://tools.ietf.org/html/rfc793#section-3.2

The state includes the TCP retransmit queue for resending lost packets

The send Sequence Space represents the bytes of data (sequence numbers to be precise):

```
                   1         2          3          4
              ----------|----------|----------|----------
                     SND.UNA    SND.NXT    SND.UNA
                                          +SND.WND
```

The numbers represent the following segments

1. old sequence numbers which have been acknowledged
2. sequence numbers of unacknowledged data
3. sequence numbers allowed for new data transmission
4. future sequence numbers which are not yet allowed

There are also the following pointers

- SND.UNA points to the first byte we have sent which has not been acknowledged by the peer
- SND.NXT points to the last byte we are going to send
  - when we send we send the bytes from SND.UNA..SND.NXT
- SND.WND points to the last byte we can send in this packet
  - TCP has this notion that the receiver can tell the sender how much they can send

ISS is the initial send sequence number

Similarly we keep track of the received bytes

- RCV.NXT - receive next
- RCV.WND - receive window
- RCV.UP  - receive urgent pointer
- IRS     - initial receive sequence number

```
                       1          2          3
                   ----------|----------|----------
                          RCV.NXT    RCV.NXT
                                    +RCV.WND

        1 - old sequence numbers which have been acknowledged
        2 - sequence numbers allowed for new reception
        3 - future sequence numbers which are not yet allowed
```

An ack is acceptable when it passes the following condition

```
SND.UNA < SEG.ACK =< SND.NXT
```

> Note that RCV.NXT is always the ACK# of the next packet we are going to send

## Initial sequence number selection

The initial sequence number is generated randomly in order to avoid confusion when connections crash and
packets arive when a new connection is reopened

## Sequence numbers in the handshake

```
1) A --> B  SYN my sequence number is X
2) A <-- B  ACK your sequence number is X
3) A <-- B  SYN my sequence number is Y
4) A --> B  ACK your sequence number is Y
```

Because 2 and 3 are sent from B to A, they can be combined. We are left with three packets which form
the three way handshake

```
      TCP A                                                TCP B

  1.  CLOSED                                               LISTEN

  2.  SYN-SENT    --> <SEQ=100><CTL=SYN>               --> SYN-RECEIVED

  3.  ESTABLISHED <-- <SEQ=300><ACK=101><CTL=SYN,ACK>  <-- SYN-RECEIVED

  4.  ESTABLISHED --> <SEQ=101><ACK=301><CTL=ACK>       --> ESTABLISHED

  5.  ESTABLISHED --> <SEQ=101><ACK=301><CTL=ACK><DATA> --> ESTABLISHED
```

To list all active connections, run

```shell
ss -tn
```

## Segments

In TCP we might receive segments which contain bytes which were already sent, so we want to make
sure that we don't store those again

Let's say we have received bytes with sequence numbers 1, 2 and 3. In our TCB SND.NXT will be set to 4
this allows us to know how many bytes to skip from the segment: SND.NXT-SEG.SEQ

`nix` is a crate which wraps low level kernel operations.
