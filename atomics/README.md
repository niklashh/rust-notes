# Atomics and Memory Ordering

Video URL: https://www.youtube.com/watch?v=rMGWeSjctlY

## Bad implementation of `mutex`

A `mutex` is a generic datatype which allows only one thread to operate
on the inner value at a time. It can do this using an atomic boolean
which is used for locking the operation to one thread.

The following program leads to data races if two threads see that
the bool is unlocked at the same time leaving the while loop.
There is nothing stopping that from happening.

```rust
use std::cell::UnsafeCell;
use std::sync::atomic::{AtomicBool, Ordering};

const LOCKED: bool = true;
const UNLOCKED: bool = false;

pub struct Mutex<T> {
    locked: AtomicBool,
    v: UnsafeCell<T>,
}

unsafe impl<T> Sync for Mutex<T> where T: Send {}

impl<T> Mutex<T> {
    pub fn new(t: T) -> Self {
        Self {
            locked: AtomicBool::new(UNLOCKED),
            v: UnsafeCell::new(t),
        }
    }

    pub fn with_lock<R>(&self, f: impl FnOnce(&mut T) -> R) -> R {
        while self.locked.load(Ordering::Relaxed) != UNLOCKED {}
        // another thread might run here
        self.locked.store(LOCKED, Ordering::Relaxed);
        // SAFETY: we hold the lock therefore we can create a mutable reference
        let r = f(unsafe { &mut *self.v.get() });
        self.locked.store(UNLOCKED, Ordering::Relaxed);
        r
    }
}

use std::thread::spawn;
fn main() {
    let l: &'static _ = Box::leak(Box::new(Mutex::new(0)));
    let handles: Vec<_> = (0..100)
        .map(|_| {
            spawn(move || {
                for _ in 0..1000 {
                    l.with_lock(|v| *v += 1)
                }
            })
        })
        .collect();
    for handle in handles {
        handle.join().unwrap();
    }

    println!("{}", l.with_lock(|v| *v));
}
```

## [`AtomicBool::compare_exchange`](https://doc.rust-lang.org/std/sync/atomic/struct.AtomicBool.html#method.compare_exchange)

Takes two values `current` and `new` and stores the `new` value to the atomic
if `current` is equal to the data in the atomic.

`compare_exchange` is a single operation doing both the read and the write
so there's not change for another thread to run in between.

The solution code:

```rust
while self
    .locked
    .compare_exchange(UNLOCKED, LOCKED, Ordering::Relaxed, Ordering::Relaxed)
    .is_err()
{}
```

If `self.locked` is locked the `compare_exchange` returns an error and the
loop keeps looping.

`compare_exchange` is an expensive call, because it coordinates exclusive
access between cores/threads. It's more expensive the more you have cores
calling it.

To make the code more efficient we can keep reading the value inside the loop
to reduce the `compare_exchange` calls:

```rust
while self
    .locked
    .compare_exchange(UNLOCKED, LOCKED, Ordering::Relaxed, Ordering::Relaxed)
    .is_err()
{
    // MESI protocol
    while self.locked.load(Ordering::Relaxed) == LOCKED {}
}
```

https://en.wikipedia.org/wiki/MESI_protocol

## `Ordering::Relaxed`

However the code still has an issue.

The following test can fail due to a data race, but it is unlikely:

```rust
#[test]
fn too_relaxed() {
    use std::sync::atomic::AtomicUsize;
    let x: &'static _ = Box::leak(Box::new(AtomicUsize::new(0)));
    let y: &'static _ = Box::leak(Box::new(AtomicUsize::new(0)));

    let t1 = spawn(move || {
        let r1 = y.load(Ordering::Relaxed); // either 0 or 42 based on whether t2 ran first
        x.store(r1, Ordering::Relaxed);
        r1
    });
    let t2 = spawn(move || {
        let r2 = x.load(Ordering::Relaxed); // has to be 0 as we haven't set y to 42 yet, right?
        y.store(42, Ordering::Relaxed);
        r2
    });
    let r1 = t1.join().unwrap();
    let r2 = t2.join().unwrap();

    eprintln!("{} {}", r1, r2);
    assert_eq!(r1 == 42 && r2 == 42, false);
}
```

Under `Ordering::Relaxed` there are basically no guarantees about what values a thread
cat read from something another thread wrote.

The values of x and y can be 0 or 42. When you load the value under `Ordering::Relaxed`
you might see any of the values written by another thread.

The compiler is allowed to re-order these operations, or the CPU can execute them
in any order, for optimization's sake, because the operations don't depend on anything
from the other.

```rust
let r2 = x.load(Ordering::Relaxed); // does not have to happen before
y.store(42, Ordering::Relaxed); // does not have to happen after
```

`Ordering::Relaxed` does not establish any order between the operations like there
clearly should be in the code.

Now to prove that our mutex is flawed:

```rust
pub fn with_lock<R>(&self, f: impl FnOnce(&mut T) -> R) -> R {
    while self
        .locked // this loop only uses self.locked
        // note that this is relaxed
        .compare_exchange_weak(UNLOCKED, LOCKED, Ordering::Relaxed, Ordering::Relaxed)
        .is_err()
    {
        while self.locked.load(Ordering::Relaxed) == LOCKED {}
    }
    let r = f(unsafe { &mut *self.v.get() }); // this operation only uses self.v
    self.locked.store(UNLOCKED, Ordering::Relaxed);
    r
}
```

Because the `self.locked` and `self.v` are different parts of the memory and
the ordering is relaxed there's nothing saying to the compiler or the CPU
that they can't reorder the operations:

```rust
pub fn with_lock<R>(&self, f: impl FnOnce(&mut T) -> R) -> R {
    // this is not fine because the lock is still unlocked
    // and multiple threads might operate on the value at the same time!
    let r = f(unsafe { &mut *self.v.get() });
    while self
        .locked // this loop only uses self.locked
        // note that this is relaxed
        .compare_exchange_weak(UNLOCKED, LOCKED, Ordering::Relaxed, Ordering::Relaxed)
        .is_err()
    {
        while self.locked.load(Ordering::Relaxed) == LOCKED {}
    }
    self.locked.store(UNLOCKED, Ordering::Relaxed);
    // this is not fine because the lock is now unlocked
    let r = f(unsafe { &mut *self.v.get() });
    r
}
```

How do we fix this?

## `Ordering::{Release, Acquire}`

https://doc.rust-lang.org/std/sync/atomic/enum.Ordering.html

The orderings release and acquire are used in the context or acquiring and releasing
a shared resource

The documentation of `Release`:

> When coupled with a **store**, all previous operations become ordered before any **load**
> of **this value** with Acquire (or stronger) ordering. In particular, all previous writes
> become visible to all threads that perform an Acquire (or stronger) load of this value.

When we do the following store using release ordering:

```rust
self.locked.store(UNLOCKED, Ordering::Release);
```

Any load of the same value `self.locked` that uses the acquire ordering (or stronger):

```rust
self.locked.load(Ordering::Acquire);
```

Must see all operations that happened before the store as having happened
before the store.

Also in the C++ reference:

> No reads or writes in the current thread can be reordered after this **store**

Basically having release ordering in the `self.locked.store` prevents `self.v.get()`
getting called after the store.

And having acquire ordering in the compare exchange forces the previous reads
and writes to become visible/ordered.

```rust
pub fn with_lock<R>(&self, f: impl FnOnce(&mut T) -> R) -> R {
    while self
        .locked
        .compare_exchange_weak(UNLOCKED, LOCKED, Ordering::Acquire, Ordering::Relaxed)
        .is_err()
    {
        // MESI protocol: stay in S when locked
        while self.locked.load(Ordering::Relaxed) == LOCKED {
            std::thread::yield_now();
        }
        std::thread::yield_now();
    }
    // SAFETY: we hold the lock therefore we can create a mutable reference
    let r = f(unsafe { &mut *self.v.get() });
    self.locked.store(UNLOCKED, Ordering::Release);
    r
}
```

The documentation of `Acquire`:

> When coupled with a **load**, if the loaded value was written by a **store** operation with Release
> (or stronger) ordering, then all subsequent operations become ordered after that store.
> In particular, all subsequent loads will see data written before the store.

Also in the C++ references:

> No reads or writes in the current thread can be reordered before this **load**

## `Ordering::AcqRel`

Do the load with acquire and store with release.

> Has the effects of both Acquire and Release together:
> For loads it uses Acquire ordering. For stores it uses the Release ordering.

Used when doing a single read and modify.

On x86_64 all operations are acquire and release so we could not reproduce the
data race bug in a test. On arm it's different

## `fetch_` methods

Instead of saying if the current value is this, set it to that, like
compare exchange, you tell the CPU how to compute the new value.

On `AtomicUsize` `fetch_add` tells the CPU to add to the value atomically,
it will never fail.

`fetch_update` takes a closure for the operation. It's implementation
is not in the CPU but a compare exchange in a while loop...

However on some architecture the "better" `fetch_add` operations may be
implemented using a compare exchange loop.

## `Ordering::SeqCst`

The following test can result in z having 1 or 2 obviously:

```rust
let x: &'static _ = Box::leak(Box::new(AtomicBool::new(false)));
let y: &'static _ = Box::leak(Box::new(AtomicBool::new(false)));
let z: &'static _ = Box::leak(Box::new(AtomicUsize::new(0)));

spawn(move || {
    x.store(true, Ordering::Release);
});
spawn(move || {
    y.store(true, Ordering::Release);
});
let t1 = spawn(move || {
    while !x.load(Ordering::Acquire) {}
    if y.load(Ordering::Acquire) {
        z.fetch_add(1, Ordering::Relaxed);
    }
});
let t2 = spawn(move || {
    while !y.load(Ordering::Acquire) {}
    if x.load(Ordering::Acquire) {
        z.fetch_add(1, Ordering::Relaxed);
    }
});
t1.join().unwrap();
t2.join().unwrap();
let z = z.load(Ordering::SeqCst);
```

But 0 is also possible:

If you load a value using acquire you will see all operations that
happened before the corresponding release store.

Let's look at t1 `x.load`: the corresponding release store happens
in `x.store` in another thread. We will see everything that happened
before the store.

When the load of y happens, it synchronize with whichever store the value it
gets (true or false) stored (can be x.store). That's not guaranteed to be a store of y!

If there was a y.store before x.store we would have to see it, but there
isn't. Therefore we are allowed to see any previous value of y: false or true.

t1 is allowed to see any value of y regardless of whether the store of
y happened.

> Note that the threads must see what the parent thread did before it.

The modification order of x and y:

```
        t2   t1,t2
MO(x): false true

        t1   t1,t2
MO(y): false true
```

t1 and t2 represent what they are allowed to see as the value of
x and y.

To get 0 the following must happen:

1. `x.store(true)` and `y.store(true)` threads execute, then
1. `t1` executes and sees `x = true`, but sees that `y = false`,
   because the load does not have to see the store of y
1. `t2` executes and sees `y = true`, but sees that `x = false`
1. mlg profit z = 420

---

Description of `SeqCst`:

> Like Acquire/Release/AcqRel (for load, store, and load-with-store operations, respectively)
> with the additional guarantee that all threads see all sequentially consistent operations in the same order.

Note that it's not operations on a given memory location, but all
operations no matter the location.

Using `SeqCst` would make z = 0 impossible.

## `std::sync::atomic::fence`

https://doc.rust-lang.org/std/sync/atomic/fn.fence.html

A load acquire always happens after a store release of the
same memory location.

Atomic fences prevent the compiler and CPU from reordering operations
by synchronizing with all other threads doing fences. Fences are not
defined by a memory location.

Fences are used to establish a happens-before relationship between
threads in a way where the happens-before is moved a little compared
to the atomic memory access.
