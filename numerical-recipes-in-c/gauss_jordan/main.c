#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "nrutil.h"

#define SWAP(a, b) { float temp=(a); (a)=(b); (b)=temp; }

void print_matrix(float **a, int n, int m) {
  for (int j = 0; j < n; ++j) {
    for (int k = 0; k < m; ++k) {
      printf("\t%f", a[j][k]);
    }
    printf("\n");
  }
}

void gaussj(float **a, int n, float **b, int m)
{
  int *indxc, *indxr, *ipiv;
  int i, icol, irow, j, k, l, ll;
  float big, dum, pivinv;

  indxc = ivector(n);
  indxr = ivector(n);
  ipiv = ivector(n);
  /* for (j = 0; j < n; ++j) ipiv[j] = 0; */
  memset(ipiv, 0, n);
  for (i = 0; i < n; ++i) { // iterate over columns in a
    big = 0.0;
    for (j = 0; j < n; ++j) {
      if (ipiv[j] != 1) {
        for (k = 0; k < n; ++k) {
          if (ipiv[k] == 0) {
            if (fabs(a[j][k]) >= big) {
              big = fabs(a[j][k]);
              irow = j;
              icol = k;
            }
          } else if (ipiv[k] > 1) nrerror("singular matrix");
        }
      }
    }
    ++(ipiv[icol]);

    if (irow != icol) {
      for (l = 0; l < n; ++l) {
        SWAP(a[irow][l], a[icol][l]);
      }
      for (l = 0; l < m; ++l) {
        SWAP(b[irow][l], b[icol][l]);
      }
    }
    
    indxr[i] = irow;
    indxc[i] = icol;
    if (a[icol][icol] == 0.0) nrerror("singular matrix 2");
    pivinv = 1.0/a[icol][icol];
    a[icol][icol] = 1.0;
    for (l = 0; l < n; ++l) a[icol][l] *= pivinv;
    for (l = 0; l < m; ++l) b[icol][l] *= pivinv;
    for (ll = 0; ll < n; ++ll) {
      if (ll != icol) {
        dum = a[ll][icol];
        a[ll][icol] = 0.0;
        for (l = 0; l < n; ++l) a[ll][l] -= a[icol][l] * dum;
        for (l = 0; l < m; ++l) b[ll][l] -= b[icol][l] * dum;
      }
    }
  }
  for (l = n - 1; l >= 0; --l) {
    if (indxr[l] != indxc[l]) {
      for (k = 0; k < n; ++k) {
        SWAP(a[k][indxr[l]], a[k][indxc[l]]);
      }
    }
  }
  free_ivector(ipiv);
  free_ivector(indxr);
  free_ivector(indxc);
}

int main(int argc, char **argv) {
  #define n 3
  #define m 1
  /* float a[n][n] = {{2.0, 2.0}, */
  /*                  {4.0,-2.0}}; */
  /* float b[n][m] = {{6.0}, */
  /*                  {0.0}}; */
  float a[n][n] = {{2.0,2.0,2.0},
                   {4.0,1.0,-2.0},
                   {-2.0,3.0,2.0}};
  float b[n][m] = {{12.0},
                   {0.0},
                   {10.0}};

  float **ap = alloca(n * sizeof(float*));
  float **bp = alloca(n * sizeof(float*));

  for (int l = 0; l < n; ++l) { // initialize pointer arrays ap, bp for each row in a and b
    ap[l] = &a[l][0];
    bp[l] = &b[l][0];
  }

  gaussj(ap, n, bp, m);

  printf("a\n");
  print_matrix(ap, n, n);

  printf("b\n");
  print_matrix(bp, n, m);

  return 0;
}
