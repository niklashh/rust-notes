#include <malloc.h>
#include <stdio.h>

void nrerror(char error_text[])
{
  void exit();

  fprintf(stderr, "Numerical Recipes runtime error...\n");
  fprintf(stderr, "%s\n", error_text);
  exit(1);
}

int *ivector(int len)
{
  int *v;
  
  v = (int *)malloc((unsigned) (len) * sizeof(int));
  if (!v) nrerror("ivector: allocation failure");
  return v;
}

void free_ivector(int *v) {
  free((char*) v);
}
