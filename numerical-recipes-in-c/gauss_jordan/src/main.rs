use gauss_jordan::*;

fn main() {
    // let a = Matrix::from_2d_array([[2.0, 2.0], [4.0, -2.0]]);

    // let b = Matrix::from_2d_array([[6.0], [0.0]]);

    let a = Matrix::from_2d_array([[2.0, 2.0, 2.0], [4.0, 1.0, -2.0], [-2.0, 3.0, 2.0]]);

    let b = Matrix::from_2d_array([[12.0], [0.0], [10.0]]);

    dbg!(gaussj(a, b));
}
