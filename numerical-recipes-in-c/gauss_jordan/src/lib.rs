use std::ops::{Index, IndexMut};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Matrix<T, const N: usize, const M: usize>([[T; M]; N]);

impl<T: Default + Copy, const N: usize, const M: usize> Matrix<T, N, M> {
    pub fn new() -> Self {
        Self([[T::default(); M]; N])
    }
}

impl<T, const N: usize, const M: usize> Matrix<T, N, M> {
    pub fn from_2d_array(arr: [[T; M]; N]) -> Self {
        Self(arr)
    }
}

impl<T, const N: usize, const M: usize> Index<usize> for Matrix<T, N, M> {
    type Output = [T; M];

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl<T, const N: usize, const M: usize> IndexMut<usize> for Matrix<T, N, M> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

macro_rules! swap {
    ($a:expr, $b:expr) => {{
        let temp = $a;
        $a = $b;
        $b = temp;
    }};
}

pub fn gaussj<const N: usize, const M: usize>(
    mut a: Matrix<f32, N, N>,
    mut b: Matrix<f32, N, M>,
) -> (Matrix<f32, N, N>, Matrix<f32, N, M>) {
    let mut indxc = [0usize; N];
    let mut indxr = [0usize; N];
    let mut ipiv = [0usize; N];

    for i in 0..N {
        let mut big = 0.0;
        let mut irow = 0;
        let mut icol = 0;

        for j in 0..N {
            if ipiv[j] != 1 {
                for k in 0..N {
                    if ipiv[k] == 0 {
                        if a[j][k].abs() >= big {
                            big = a[j][k].abs();
                            irow = j;
                            icol = k;
                        }
                    } else if ipiv[k] > 1 {
                        panic!("singular matrix");
                    }
                }
            }
        }
        ipiv[icol] += 1;

        if irow != icol {
            for l in 0..N {
                swap!(a[irow][l], a[icol][l])
            }
            for l in 0..M {
                swap!(b[irow][l], b[icol][l])
            }
        }

        indxr[i] = irow;
        indxc[i] = icol;
        if a[icol][icol] == 0.0 {
            panic!("singular matrix 2");
        }
        let pivinv = 1.0 / a[icol][icol];
        a[icol][icol] = 1.0;

        for l in 0..N {
            a[icol][l] *= pivinv;
        }
        for l in 0..M {
            b[icol][l] *= pivinv;
        }
        for ll in 0..N {
            if ll != icol {
                let dum = a[ll][icol];
                a[ll][icol] = 0.0;

                for l in 0..N {
                    a[ll][l] -= a[icol][l] * dum;
                }
                for l in 0..M {
                    b[ll][l] -= b[icol][l] * dum;
                }
            }
        }
    }
    for l in (0..N).rev() {
        if indxr[l] != indxc[l] {
            for k in 0..N {
                swap!(a[k][indxr[l]], a[k][indxc[l]]);
            }
        }
    }

    (a, b)
}
