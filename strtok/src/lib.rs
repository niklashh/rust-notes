pub fn strtok<'borrow, 'data>(s: &'borrow mut &'data str, delimiter: char) -> &'data str
where
    'data: 'borrow,
{
    if let Some(i) = s.find(delimiter) {
        let prefix = &s[..i];
        let suffix = &s[(i + delimiter.len_utf8())..];
        *s = suffix;
        prefix
    } else {
        let prefix = *s;
        *s = "";
        prefix
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut x: &'static str = "hello world";
        let hello = strtok(&mut x, ' ');
        assert_eq!(x, "world");
        drop(x);
        assert_eq!(hello, "hello");
    }
}
