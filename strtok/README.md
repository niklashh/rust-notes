# Subtyping and Variance

Video url: https://www.youtube.com/watch?v=b4mS5UPHh20


## `strtok`

C++ documentation: https://www.cplusplus.com/reference/cstring/strtok/

The type definition of `strtok` in C++:

```cpp
char * strtok ( char * str, const char * delimiters );
```

in rust (with only one delimiter):

```rust
(&mut &str, char) -> &str
```

`strtok` cuts the string at the first occurrence of the delimiter. It
returns the first part while mutating the pointer to point to the character
after the delimiter.

The code and a test for this:

```rust
pub fn strtok<'a>(s: &'a mut &'a str, delimiter: char) -> &'a str {
    if let Some(i) = s.find(delimiter) {
        let prefix = &s[..i];
        let suffix = &s[(i + delimiter.len_utf8())..];
        *s = suffix;
        prefix
    } else {
        let prefix = *s;
        *s = "";
        prefix
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut x = "hello world";
        {
            let hello = strtok(&mut x, ' ');
            assert_eq!(hello, "hello");
        }
        assert_eq!(x, "world");
    }
}
```

The compiler is giving an error:

```rust
   | {
22 |     let hello = strtok(&mut x, ' ');
   |                        ------ mutable borrow occurs here
   | }
25 | assert_eq!(x, "world");
   | ^^^^^^^^^^^^^^^^^^^^^^^
   | |
   | immutable borrow occurs here
   | mutable borrow later used here
```

Which doesn't make much sense because hello is dropped at the end of the scope
and no exclusive references to x seem to exist at line 25.

You think it's related to the lifetime of `hello`?

Even if the return value has a static lifetime, the code fails to compile:

```rust
pub fn strtok<'a>(s: &'a mut &'a str, delimiter: char) -> &'static str {
    if let Some(i) = s.find(delimiter) {
        let prefix = &s[..i];
        let suffix = &s[(i + delimiter.len_utf8())..];
        *s = suffix;
        ""
    } else {
        let prefix = *s;
        *s = "";
        ""
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut x = "hello world";
        {
            let hello = strtok(&mut x, ' ');
            assert_eq!(hello, "hello");
        }
        assert_eq!(x, "world");
    }
}
```

The error is exactly the same. Even if the return value is void...

### Variance

Let's look closely at the types here:

```rust
strtok: (&'a mut &'a str, ...) -> ...;
x: &mut &'static str; // a string literal is static
```

The compiler deduces that `'a` is static and therefore the lifetime of the
exclusive reference must be static:

```rust
x: &mut &'static str; // =>
x: &'static mut &'static str;
```

The lifetime of `x` is as long as it lives
on the stack, but calling `strtok` made the
compiler think that the reference has a static lifetime.
The error says that `x` is borrowed mutably which is
correct as its lifetime is static.

When the `strtok` function is made to return nothing
and the assert is removed the code compiles:

```rust
pub fn strtok<'a>(s: &'a mut &'a str, delimiter: char) {
    if let Some(i) = s.find(delimiter) {
        let prefix = &s[..i];
        let suffix = &s[(i + delimiter.len_utf8())..];
        *s = suffix;
    } else {
        let prefix = *s;
        *s = "";
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let mut x = "hello world";
        strtok(&mut x, ' ');
        // assert_eq!(x, "world");
    }
}
```

The lifetime mismatch is still there, but the compiler has a trick:
The compiler pretends that the lifetime of `x` is not static
but rather the liftime of the function.

If you have a static reference, you can use it in place of any reference
that has a shorter lifetime.

An example of code that also does this:

```rust
fn main() {
    let s = String::new();
    let x: &'static str = "hello world";
    let mut y = &*s; // y has a lifetime of main
    y = x; // y can hold the value of x because y dies before the static data
}
```

This type of variance is called _covariance_.

If we force the lifetime of `x` to be static so that the compiler can't
shorten it:

```rust
fn check_is_static(_: &'static str) {}

let mut x = "hello world";
check_is_static(x);
strtok(&mut x, ' ');
```

The compiler gives an error:

```rust
32 |         strtok(&mut x, ' ');
   |         -------^^^^^^------
   |         |      |
   |         |      borrowed value does not live long enough
   |         argument requires that `x` is borrowed for `'static`
33 |         // assert_eq!(x, "world");
34 |     }
   |     - `x` dropped here while still borrowed
```

Because `x` is static, the exclusive reference must be too.
Notice that `&mut x` is referencing `x` rather than the static data,
and can't live longer than `x`.

Replacing the call to `strtok` with

```rust
strtok(&mut &*x, ' ');
```

solves the problem because it's now a reference to the static data
that `x` is referencing, rather than a reference of `x`.

Accessing `x` after calling `strtok` immutably violates borrow-
rules because the exclusive reference doesn't get dropped
but lives as long as `x`.

## Subtypes

Chapter in the rustonomicon: https://doc.rust-lang.org/nomicon/subtyping.html

`'static` is a subtype of any `'a` because
it at least as "useful" as `'a`.

It's not true that all subtypes can be used in place
of their parents.

The relation is written as

```
'static: 'a
T: U
Cat: Animal
```

There are 3 types of variance:

### Covariance

You are allowed to provide any type that is a subtype of the parameter.
The type of the parameter is covariant

```rust
fn foo(&str) {}

foo(&'a str) // works
foo(&'static str) // also works because of subtyping
```

### Contravariance

In this example the argument `&'a str` is contravariant, because you can't
require the function to take a more "useful" type, like `&'static str`

```
fn foo(bar: Fn(&'a str) -> ()) {
    bar("") // imagine that the "" has a lifetime of 'a which is shorter than 'static
}

let bar: Fn(&'static str) -> ();
// calling foo with a function (bar) which requires a static str
// is more strict as it can't be passed some str with any lifetime 'a
// you can't give foo a function with stricter requirements for its arguments
foo(bar);
```

```
&'static str // more useful, can be applied in more cases
&'a str

Fn(&'static str)
Fn(&'a str) // more useful, more applicable
```

This is the only case where contravariance shows up in rust

```
// 'static <: 'a // a subtype

// References are covariant in their lifetimes, so the following is implied
// &'static T <: &'a T
```

```
// 'static <: 'a

// A function with a parameter with a specific lifetime is a subtype of
// a function with a parameter with a static lifetime
// Fn(&'a T) <: Fn(&'static T)
```

In short: if the parameter of a function is more useful, the function
is less useful

### Invariance

Mutable references `&'a mut T` are invariant in their inner type `T`
but covariant in their lifetime `&'a`

If mutable references were covariant in their inner type we could stick something less
useful in it and still try to use it as what it was before (something more useful).

The following code compiles:

```rust
let mut y = true;
let mut z /* &'y mut bool */ = &mut y;

let x = Box::new(true);
let x: &'static mut bool = Box::leak(x);

z = x; // &'y mut bool = &'static mut bool
```

## Fixing `strtok`

We need to add another lifetime parameter because we don't want the lifetime of
the exclusive reference (scope) to be the lifetime of the data (static):

```rust
pub fn strtok<'borrow, 'data>(s: &'borrow mut &'data str, delimiter: char) -> &'data str
```

The lifetime of data `&'static str` is always longer than the
lifetime of the borrow `&'borrow` (dies at the end of the function).

We could also make the function return a string with the lifetime of the borrow:

```rust
... -> &'borrow str
```

But then this code wouldn't compile:

```rust
let mut x: &'static str = "hello world";
let hello = strtok(&mut x, ' ');
assert_eq!(x, "world");
drop(x);
assert_eq!(hello, "hello");
```

It naturally makes sense to give the output the longest lifetime possible although
it's not required...

Because mutable references are covariant in their lifetimes, the compiler can
shorten the lifetime of `z` to die before `x` is borrowed again:

```rust
let mut x: &'static str = "hello world";
let z = &mut x;
// z gets dropped immediately
let hello = strtok(&mut x, ' ');
assert_eq!(x, "world");
drop(x);
assert_eq!(hello, "hello");
```

The lifetime subtyping can be explicitly written in the trait bounds:

```rust
pub fn strtok<'borrow, 'data>(s: &'borrow mut &'data str, delimiter: char) -> &'data str
where
    'data: 'borrow,
```
