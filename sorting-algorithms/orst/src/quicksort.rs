use super::Sorter;

pub struct QuickSort;

fn quicksort<T: Ord>(slice: &mut [T]) {
    match slice.len() {
        0 | 1 => return,
        // TODO bench this
        // 2 => {
        //     if slice[0] > slice[1] {
        //         slice.swap(0, 1);
        //     }
        //     return;
        // }
        _ => {}
    }

    let (pivot, rest) = slice.split_first_mut().expect("slice is non-empty");
    let mut left = 0;
    let mut right = rest.len(); // don't subtract 1 to avoid underflow
    while left < right {
        if &rest[left] <= pivot {
            // left already correct
            left += 1;
        } else if &rest[right - 1] > pivot {
            // right already correct
            right -= 1;
        } else {
            // the element under left is on the wrong side
            // and the element under right is on the wrong side
            // swapping is efficient and we can step both counters
            rest.swap(left, right - 1);
            left += 1;
            right -= 1;
        }
    }

    // adjust for the pivot at 0 in slice which is not in rest
    let left = left + 1;

    // place the pivot at its final location = left - 1
    slice.swap(0, left - 1);

    // right slice contains the pivot
    let (left, right) = slice.split_at_mut(left - 1);
    assert!(left.last() <= right.last());
    quicksort(left);
    // don't include the pivot in the next quicksort for right
    quicksort(&mut right[1..]);
}

impl Sorter for QuickSort {
    fn sort<T>(&self, slice: &mut [T])
    where
        T: Ord,
    {
        quicksort(slice)
    }
}

#[test]
fn sort_mid() {
    let mut things = vec![3, 2, 4, 1];
    QuickSort.sort(&mut things);
    assert_eq!(things, [1, 2, 3, 4]);
}

#[test]
fn sort_min() {
    let mut things = vec![1, 4, 2, 3];
    QuickSort.sort(&mut things);
    assert_eq!(things, [1, 2, 3, 4]);
}

#[test]
fn sort_max() {
    let mut things = vec![4, 3, 1, 2];
    QuickSort.sort(&mut things);
    assert_eq!(things, [1, 2, 3, 4]);
}

#[test]
fn sort_empty() {
    let mut empty: Vec<()> = vec![];
    QuickSort.sort(&mut empty);
    assert_eq!(empty, []);
}
