use super::Sorter;

pub struct InsertionSort {
    pub smart: bool,
}

impl Sorter for InsertionSort {
    fn sort<T>(&self, slice: &mut [T])
    where
        T: Ord,
    {
        // [ sorted | not sorted ]
        for unsorted in 1..slice.len() {
            // slice[unsorted..] is not sorted
            // take slice[unsorted] and place it in
            // sorted location in slice[..=unsorted]
            // [ 4 | 2 3 1 ]
            // [ 2 4 | 3 1 ]
            // [ 2 3 4 | 1 ]
            // [ 1 2 3 4 | ]
            // an easy way to move the next unsorted element to it's place
            // would be to swap it with it's left neighbor until it's in
            // the correct place

            if !self.smart {
                let mut i = unsorted;
                while i > 0 && slice[i - 1] > slice[i] {
                    slice.swap(i - 1, i);
                    i -= 1;
                }
            } else {
                let i = match slice[..unsorted].binary_search(&slice[unsorted]) {
                    Ok(i) | Err(i) => i,
                };
                slice[i..=unsorted].rotate_right(1);
            }
        }
    }
}

#[test]
fn sort() {
    let mut things = vec![4, 2, 3, 1];
    InsertionSort { smart: false }.sort(&mut things);
    assert_eq!(things, [1, 2, 3, 4]);
}

#[test]
fn sort_alot() {
    let mut things: Vec<usize> = (0..100).rev().collect();
    let mut things_in_order = things.clone();
    <[_]>::sort(&mut things_in_order[..]);
    assert_ne!(things, things_in_order);
    InsertionSort { smart: true }.sort(&mut things);
    assert_eq!(things, things_in_order);
}

#[test]
fn sort_empty() {
    let mut empty: Vec<()> = vec![];
    InsertionSort { smart: true }.sort(&mut empty);
    assert_eq!(empty, []);
}
