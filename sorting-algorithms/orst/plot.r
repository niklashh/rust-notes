#!/usr/bin/rscript

library(ggplot2)
t <- read.table('bench.dat', header=TRUE)

ggplot(t, aes(n, time, colour = algorithm)) + geom_point() +scale_y_log10() +scale_x_log10() +geom_smooth()

ggsave(file = "bench.pdf")
