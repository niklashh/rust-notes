= Quick Handy Rust Snippets

== Check Rust compiler version from within

```rust
let rustc = std::env::var_os("RUSTC").unwrap_or_else(|| std::ffi::OsString::from("rustc"));
let mut cmd = if let Some(wrapper) = std::env::var_os("RUSTC_WRAPPER").filter(|w| !w.is_empty()) {
    let mut cmd = std::process::Command::new(wrapper);
    cmd.arg(rustc);
    cmd
} else {
    std::process::Command::new(rustc)
};

dbg!(cmd.arg("-vV").output().unwrap());
```