# Procedural Macros

Video URL: https://www.youtube.com/watch?v=geovSK3wMB8

## `derive` example

```rust
#[derive(Serialize, Deserialize)]
struct Foo {
 	bar: usize,
}
```

The `derive` macro invokes a program which generates the required pieces of code
to implement `Serialize` and `Deserialize` for `Foo`.

## `test` example

```rust
#[test]
fn foo() {
}
```

`test` is an attribute macro which transforms the following code into a test.

Usually attribute macros take parameters like the rocket.rs `route` macro:

```rust
#[route(get, "/")]
fn get() {
}
```

## Types of procedural macros

https://doc.rust-lang.org/nightly/reference/procedural-macros.html

### Function-like procedural macros

Fu[1;2Anction-like procedural macros are similar to declarative macros because they replace
the block of code. Function-like macros have a signature of `(TokenStream) -> TokenStream`.

### Derive macros

Derive macros extend the standard library's `derive` macro attributes. Derive macros
don't replace their input token stream like function-like procedural macros do.

### Attribute macros

Attribute macros are given the token stream of the attributes, like `get, "/"` in the
`route` example, and also take in the token stream of the annotated code.

Items in rust

## Items

https://doc.rust-lang.org/nightly/reference/items.html

Items are components of source code which often contain a block defined by curly braces.
Although `use` statements are also items.

## Workshop

https://github.com/dtolnay/proc-macro-workshop

### Builder

Clone the workshop and go to the `builder` subfolder. We will do our work there.

### Syn crate

https://github.com/dtolnay/syn

`syn` is used to parse rust source code.

`DeriveInput` gives access to all tokens that are in the annotated block of code.

Uncommenting the parse test in `tests/progress.rs` and running the tests in the builder
results in an error:

```
test tests/01-parse.rs ... error
┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈
error: proc-macro derive panicked
  --> $DIR/01-parse.rs:26:10
   |
26 | #[derive(Builder)]
   |          ^^^^^^^
   |
   = help: message: not implemented
┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈
```

We need to add `syn` to builder:

```shell
cargo add syn
```

### 01 Parse

The lib.rs looks like:

```rust
extern crate proc_macro;

use proc_macro::TokenStream;

#[proc_macro_derive(Builder)]
pub fn derive(input: TokenStream) -> TokenStream {
    let _ = input;

    unimplemented!()
}
```

Procedural macros don't return a `result`, but are always assumed to produce a TokenStream.

A panic is considered a failure to the compiler.

Let's use the `parse_macro_input` macro from `syn` to create the abstract syntax tree:

```rust
use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_derive(Builder)]
pub fn derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    eprintln!("{:#?}", ast);

    TokenStream::new()
}
```

Now running the tests prints out the syntax tree and the test passes.

### 02 Create Builder

Use the `quote` library to expand some syntax into a template code block:

```rust
use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_derive(Builder)]
pub fn derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    let name = &ast.ident;
    let bname = format!("{}Builder", name);
    let bident = syn::Ident::new(&bname, name.span());
    let expanded = quote! {
    struct #bident {
    }
    impl #name {
        fn builder() -> #bident {
            #bident {
            }
        }
    }
    };
    expanded.into()
}
```

To debug the behavior, we can use cargo expand.

First add the 02 test into `main.rs`:

```rust
use derive_builder::Builder;

#[derive(Builder)]
pub struct Command {
    executable: String,
    args: Vec<String>,
    env: Vec<String>,
    current_dir: String,
}

fn main() {
    let builder = Command::builder();

    let _ = builder;
}
```

Now `cargo expand` shows:

```rust
#![feature(prelude_import)]
#[prelude_import]
use std::prelude::v1::*;
#[macro_use]
extern crate std;
use derive_builder::Builder;
pub struct Command {
    executable: String,
    args: Vec<String>,
    env: Vec<String>,
    current_dir: String,
}
struct CommandBuilder {}
impl Command {
    fn builder() -> CommandBuilder {
        CommandBuilder {}
    }
}
fn main() {
    let builder = Command::builder();
    let _ = builder;
}
```

And let's add the struct fields that the comments in the test asked for:

```rust
// ... quote!
pub struct #bident {
    executable: Option<String>,
    args: Option<Vec<String>>,
    env: Option<Vec<String>>,
    current_dir: Option<String>,
}
impl #name {
    fn builder() -> #bident {
        #bident {
            executable: None,
            args: None,
            env: None,
            current_dir: None,
        }
    }
}
```

### 03 Call Setters

Now we want to add some methods for the builder:

```rust
// pub struct #bident { ... }
impl #bident {
    pub fn executable(&mut self, executable: String) -> &mut Self {
        self.executable = Some(executable);
        self
    }
    pub fn args(&mut self, args: Vec<String>) -> &mut Self {
        self.args = Some(args);
        self
    }
    pub fn env(&mut self, env: Vec<String>) -> &mut Self {
        self.env = Some(env);
        self
    }
    pub fn current_dir(&mut self, current_dir: String) -> &mut Self {
        self.current_dir = Some(current_dir);
        self
    }
}
// impl #name
```

> This also solves 05 Method Chaining

And the test passes.

Output of cargo expand from the test:

```rust
#![feature(prelude_import)]
#[prelude_import]
use std::prelude::v1::*;
#[macro_use]
extern crate std;
use derive_builder::Builder;
pub struct Command {
    executable: String,
    args: Vec<String>,
    env: Vec<String>,
    current_dir: String,
}
pub struct CommandBuilder {
    executable: Option<String>,
    args: Option<Vec<String>>,
    env: Option<Vec<String>>,
    current_dir: Option<String>,
}
impl CommandBuilder {
    pub fn executable(&mut self, executable: String) -> &mut Self {
        self.executable = Some(executable);
        self
    }
    pub fn args(&mut self, args: Vec<String>) -> &mut Self {
        self.args = Some(args);
        self
    }
    pub fn env(&mut self, env: Vec<String>) -> &mut Self {
        self.env = Some(env);
        self
    }
    pub fn current_dir(&mut self, current_dir: String) -> &mut Self {
        self.current_dir = Some(current_dir);
        self
    }
}
impl Command {
    fn builder() -> CommandBuilder {
        CommandBuilder {
            executable: None,
            args: None,
            env: None,
            current_dir: None,
        }
    }
}
fn main() {
    let mut builder = Command::builder();
    builder.executable("cargo".to_owned());
    builder.args(<[_]>::into_vec(box [
        "build".to_owned(),
        "--release".to_owned(),
    ]));
    builder.env(::alloc::vec::Vec::new());
    builder.current_dir("..".to_owned());
}
```

### 04 Call Build

Now we want to implement the `build` method for the builder macro:

```rust
pub fn build(&mut self) -> Result<#name, Box<dyn ::std::error::Error>> {
    Ok(#name {
        executable: self.executable.clone().ok_or("executable is not set")?,
        args: self.args.clone().ok_or("args is not set")?,
        env: self.env.clone().ok_or("env is not set")?,
        current_dir: self.current_dir.clone().ok_or("current_dir is not set")?,
    })
}
```

### 05 Method Chaining

Done in 03

### Iterating over the fields dynamically

```rust
// ...
let fields = if let syn::Data::Struct(syn::DataStruct {
    fields: syn::Fields::Named(syn::FieldsNamed { ref named, .. }),
    ..
}) = ast.data
{
    named
} else {
    // Getting fields dynamically implemented only for structs
    unimplemented!();
};
let optionized = fields.iter().map(|f| {
    let name = &f.ident;
    let ty = &f.ty;
    quote! {
        #name: ::std::option::Option<#ty>
    }
});
let methods = fields.iter().map(|f| {
    let name = &f.ident;
    let ty = &f.ty;
    quote! {
        pub fn #name(&mut self, #name: #ty) -> &mut Self {
            self.#name = Some(#name);
            self
        }
    }
});
let build_fields = fields.iter().map(|f| {
    let name = &f.ident;
    quote! {
        #name: self.#name.clone().ok_or(concat!(stringify!(#name), " is not set"))?
    }
});
let build_empty = fields.iter().map(|f| {
    let name = &f.ident;
    quote! { #name: None }
});
let expanded = quote! {
    pub struct #bident {
        #(#optionized,)*
    }
    impl #bident {
        #(#methods)*

        pub fn build(&self) -> Result<#name, Box<dyn ::std::error::Error>> {
            Ok(#name {
                #(#build_fields,)*
            })
        }
    }
    impl #name {
        fn builder() -> #bident {
            #bident {
                #(#build_empty,)*
            }
        }
    }
};
expanded.into()
```

### 06

The task is to allow omitting a build argument based on whether the field is an Option or not

The builder should still have all the fields as Options in order to not initialize them with
garbage.

A function that extracts the type parameter for Option needs to be created:

```rust
fn ty_inner_type(ty: &syn::Type) -> Option<&syn::Type> {
    if let syn::Type::Path(ref p) = ty {
        if p.path.segments.len() != 1 || p.path.segments[0].ident != "Option" {
            return None;
        }

        if let syn::PathArguments::AngleBracketed(ref inner_ty) = p.path.segments[0].arguments {
            if inner_ty.args.len() != 1 {
                return None;
            }

            let inner_ty = inner_ty.args.first().unwrap();
            if let syn::GenericArgument::Type(ref t) = inner_ty {
                return Some(t);
            }
        }
    }
    None
}
```

We can then use that function not only to get the generic type, but also to check if a type
is an Option or not:

```rust
let optionized = fields.iter().map(|f| {
    let name = &f.ident;
    let ty = &f.ty;
    if ty_inner_type(ty).is_some() {
        quote! { #name: #ty }
    } else {
        quote! {
            #name: ::std::option::Option<#ty>
        }
    }
});
let methods = fields.iter().map(|f| {
    let name = &f.ident;
    let ty = &f.ty;
    if let Some(inner_ty) = ty_inner_type(ty) {
        quote! {
           pub fn #name(&mut self, #name: #inner_ty) -> &mut Self {
               self.#name = Some(#name);
               self
           }
        }
    } else {
        quote! {
            pub fn #name(&mut self, #name: #ty) -> &mut Self {
                self.#name = Some(#name);
                self
            }
        }
    }
});
let build_fields = fields.iter().map(|f| {
    let name = &f.ident;
    if ty_inner_type(&f.ty).is_some() {
        quote! {
            #name: self.#name.clone()
        }
    } else {
        quote! {
            #name: self.#name.clone().ok_or(concat!(stringify!(#name), " is not set"))?
        }
    }
});
```

### 07 Repeated Fields

The task is to allow running e.g. `.arg` repeatedly which keeps extending the args.

We need to add _inert_ attributes to our proc macro in order to support the following syntax:

```rust
#[derive(Builder)]
pub struct Command {
    Executable: String,
    #[builder(each = "arg")]
    args: Vec<String>,
    #[builder(each = "env")]
    env: Vec<String>,
    current_dir: Option<String>,
}
```

In order to do this, we add `attributes(builder)` to our proc macro initialization:

```rust
#[proc_macro_derive(Builder, attributes(builder))]
```

`proc_macro2` is a crate that is a wrapper around the compiler's `proc_macro` crate. It allows
using proc macro code outside proc macros.

To parse the attributes for every field:

```rust
// ...
let extend_methods = fields.iter().map(|f| {
    for attr in &f.attrs {
        if attr.path.segments.len() == 1 && attr.path.segments[0].ident == "builder" {
            if let Some(TokenTree::Group(g)) = attr.tokens.clone().into_iter().next() {
                let mut tokens = g.stream().into_iter();
                match tokens.next().unwrap() {
                    TokenTree::Ident(ref i) => assert_eq!(i, "each"),
                    tt => panic!("expected 'each', found {}", tt),
                }
                match tokens.next().unwrap() {
                    TokenTree::Punct(ref p) => assert_eq!(p.as_char(), '='),
                    tt => panic!("expected '=', found {}", tt),
                }
                let arg = match tokens.next().unwrap() {
                    TokenTree::Literal(l) => l,
                    tt => panic!("expected string, found {}", tt),
                };
                match syn::Lit::new(arg) {
                    syn::Lit::Str(s) => {
                        let arg = syn::Ident::new(&s.value(), s.span());
                        let inner_ty = ty_inner_type("Vec", &f.ty).unwrap();
                        return Some(quote! {
                            pub fn #arg(&mut self, #arg: #inner_ty) -> &mut Self {
                                if let Some(ref mut values) = self.#arg {
                                    values.push(#arg);
                                } else {
                                    self.#name = Some(vec![#arg]);
                                }
                            }
                        });
                    }
                    lit => panic!("expected string, found {:?}", lit),
                }
            }
        }
    }
    None
});
// ...
#(#methods)*
#(#extend_methods)*

// pub fn build
```

But this leads to the compiler error that multiple functions have the same name `env`.
This is because the `each = "env"` attribute is saying that the new method, for pushing
values into the vector, is called `env`. We want there to only be this method if a conflict
would follow otherwise.

Code at this point:

```rust
use proc_macro::TokenStream;
use proc_macro2::TokenTree;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_derive(Builder, attributes(builder))]
pub fn derive(input: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(input as DeriveInput);
    let name = &ast.ident;
    let bname = format!("{}Builder", name);
    let bident = syn::Ident::new(&bname, name.span());
    let fields = if let syn::Data::Struct(syn::DataStruct {
        fields: syn::Fields::Named(syn::FieldsNamed { ref named, .. }),
        ..
    }) = ast.data
    {
        named
    } else {
        // Getting fields dynamically implemented only for structs
        unimplemented!();
    };

    let optionized = fields.iter().map(|f| {
        let name = &f.ident;
        let ty = &f.ty;
        if ty_inner_type("Option", ty).is_some() || builder_of(&f).is_some() {
            quote! { #name: #ty }
        } else {
            quote! {
                #name: ::std::option::Option<#ty>
            }
        }
    });
    let methods = fields.iter().map(|f| {
        let name = &f.ident;
        let ty = &f.ty;
        let set_method = if let Some(inner_ty) = ty_inner_type("Option", ty) {
            quote! {
               pub fn #name(&mut self, #name: #inner_ty) -> &mut Self {
                   self.#name = Some(#name);
                   self
               }
            }
        } else if builder_of(&f).is_some() {
            quote! {
            pub fn #name(&mut self, #name: #ty) -> &mut Self {
                self.#name = #name;
                self
            }
                     }
        } else {
            quote! {
                pub fn #name(&mut self, #name: #ty) -> &mut Self {
                    self.#name = Some(#name);
                    self
                }
            }
        };

        match extend_method(&f) {
            None => set_method.into(),
            Some((true, extend_method)) => extend_method,
            Some((false, extend_method)) => {
                let expr = quote! {
                    #set_method
                    #extend_method
                };
                expr.into()
            }
        }
    });
    let build_fields = fields.iter().map(|f| {
        let name = &f.ident;
        if ty_inner_type("Option", &f.ty).is_some() || builder_of(&f).is_some() {
            quote! {
                #name: self.#name.clone()
            }
        } else {
            quote! {
                #name: self.#name.clone().ok_or(concat!(stringify!(#name), " is not set"))?
            }
        }
    });
    let build_empty = fields.iter().map(|f| {
        let name = &f.ident;
        if builder_of(&f).is_some() {
            quote! { #name: Vec::new() }
        } else {
            quote! { #name: None }
        }
    });
    let expanded = quote! {
        pub struct #bident {
            #(#optionized,)*
        }
        impl #bident {
            #(#methods)*

            pub fn build(&self) -> Result<#name, Box<dyn ::std::error::Error>> {
                Ok(#name {
                    #(#build_fields,)*
                })
            }
        }
        impl #name {
            fn builder() -> #bident {
                #bident {
                    #(#build_empty,)*
                }
            }
        }
    };
    expanded.into()
}

fn builder_of(f: &syn::Field) -> Option<proc_macro2::Group> {
    for attr in &f.attrs {
        if attr.path.segments.len() == 1 && attr.path.segments[0].ident == "builder" {
            if let TokenTree::Group(g) = attr.tokens.clone().into_iter().next().unwrap() {
                return Some(g);
            }
        }
    }
    None
}

fn extend_method(f: &syn::Field) -> Option<(bool, proc_macro2::TokenStream)> {
    let name = f.ident.as_ref().unwrap();
    let g = builder_of(f)?;
    let mut tokens = g.stream().into_iter();
    match tokens.next().unwrap() {
        TokenTree::Ident(ref i) => assert_eq!(i, "each"),
        tt => panic!("expected 'each', found {}", tt),
    }
    match tokens.next().unwrap() {
        TokenTree::Punct(ref p) => assert_eq!(p.as_char(), '='),
        tt => panic!("expected '=', found {}", tt),
    }
    let arg = match tokens.next().unwrap() {
        TokenTree::Literal(l) => l,
        tt => panic!("expected string, found {}", tt),
    };

    match syn::Lit::new(arg) {
        syn::Lit::Str(s) => {
            let arg = syn::Ident::new(&s.value(), s.span());
            let inner_ty = ty_inner_type("Vec", &f.ty).unwrap();
            let method = quote! {
                pub fn #arg(&mut self, #arg: #inner_ty) -> &mut Self {
                    self.#name.push(#arg);
                    self
                }
            };
            Some((&arg == name, method))
        }
        lit => panic!("expected string, found {:?}", lit),
    }
}

fn ty_inner_type<'a>(wrapper: &str, ty: &'a syn::Type) -> Option<&'a syn::Type> {
    if let syn::Type::Path(ref p) = ty {
        if p.path.segments.len() != 1 || p.path.segments[0].ident != wrapper {
            return None;
        }

        if let syn::PathArguments::AngleBracketed(ref inner_ty) = p.path.segments[0].arguments {
            if inner_ty.args.len() != 1 {
                return None;
            }

            let inner_ty = inner_ty.args.first().unwrap();
            if let syn::GenericArgument::Type(ref t) = inner_ty {
                return Some(t);
            }
        }
    }
    None
}
```

### 08 Unrecognized Attribute

The task is to make the compiler show a nice error message for the following code:

```rust
#[derive(Builder)]
pub struct Command {
    executable: String,
    #[builder(eac = "arg")]
    args: Vec<String>,
    env: Vec<String>,
    current_dir: Option<String>,
}
```

Which should produce the following error

```rust
error: expected `builder(each = "...")`
  --> main.rs:14:7
   |
14 |     #[builder(eac = "arg")]
   |       ^^^^^^^^^^^^^^^^^^^^
```

To do this we return a `syn::Error` instead of panicking from `extend_method`. Solution:

```rust
fn builder_of(f: &syn::Field) -> Option<&syn::Attribute> {
    for attr in &f.attrs {
        if attr.path.segments.len() == 1 && attr.path.segments[0].ident == "builder" {
            return Some(attr);
        }
    }
    None
}

fn extend_method(f: &syn::Field) -> Option<(bool, proc_macro2::TokenStream)> {
    let name = f.ident.as_ref().unwrap();
    let g = builder_of(f)?;
    let err = |span| {
        Some((
            false,
            syn::Error::new(span, "expected `builder(each = \"...\")`").to_compile_error(),
        ))
    };
    let meta = match g.parse_meta() {
        Ok(syn::Meta::List(mut nvs)) => {
            assert!(nvs.path.is_ident("builder"));
            if nvs.nested.len() != 1 {
                return err(nvs.span());
            };
            match nvs.nested.pop().unwrap().into_value() {
                syn::NestedMeta::Meta(syn::Meta::NameValue(nv)) => {
                    if !nv.path.is_ident("each") {
                        return err(nvs.span());
                    }
                    nv
                }
                meta => return err(meta.span()),
            }
        }
        Ok(meta) => return err(meta.span()),
        Err(e) => return Some((false, e.to_compile_error())),
    };

    match meta.lit {
        syn::Lit::Str(s) => {
            let arg = syn::Ident::new(&s.value(), s.span());
            let inner_ty = ty_inner_type("Vec", &f.ty).unwrap();
            let method = quote! {
                pub fn #arg(&mut self, #arg: #inner_ty) -> &mut Self {
                    self.#name.push(#arg);
                    self
                }
            };
            Some((&arg == name, method))
        }
        lit => panic!("expected string, found {:?}", lit),
    }
}
```

### 09 Redefined Prelude Types

The task is to be hygienic about types, so we should use absolute paths:

```rust
Option => ::std::option::Option
```

## `seq!`

### 01 Parse Input

The task is to get the following code to compile:

```rust
use seq::seq;

seq!(N in 0..8 {
    // nothing
});
```

Solution:

```rust
extern crate proc_macro;

use proc_macro::TokenStream;
use syn::parse::{Parse, ParseStream};
use syn::{parse_macro_input, Result, Token};

#[derive(Debug)]
struct SeqMacroInput {}

impl Parse for SeqMacroInput {
    fn parse(input: ParseStream) -> Result<Self> {
        let var = syn::Ident::parse(input)?;
        dbg!(var);
        let _in = <Token![in]>::parse(input)?;
        let from = syn::Lit::parse(input)?;
        let _dots = <Token![..]>::parse(input)?;
        let to = syn::Lit::parse(input)?;
        let body = syn::Block::parse(input)?;
        dbg!(from, to, body);
        Ok(SeqMacroInput {})
    }
}
#[proc_macro]
pub fn seq(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as SeqMacroInput);
    dbg!(input);

    TokenStream::new()
}
```

The output when running:

```rust
[seq/src/lib.rs:13] var = Ident {
    ident: "N",
    span: #0 bytes(165..166),
}
[seq/src/lib.rs:19] from = Int(
    LitInt {
        token: 0,
    },
)
[seq/src/lib.rs:19] to = Int(
    LitInt {
        token: 8,
    },
)
[seq/src/lib.rs:19] body = Block {
    brace_token: Brace,
    stmts: [],
}
```

### 02 Parse Body

```rust
let braces = syn::braced!(content in input);

dbg!(from, to, braces);
let tt = proc_macro2::TokenStream::parse(&content)?;
dbg!(tt);
```

### 03 Expand Four Errors

The task is to produce four compiler errors with incrementing numbers:

```rust
use seq::seq;

seq!(N in 0..4 {
    compile_error!(concat!("error number ", stringify!(N)));
});

fn main() {}
```

```rust
error: error number 0
  --> $DIR/03-expand-four-errors.rs:20:5
   |
20 |     compile_error!(concat!("error number ", stringify!(N)));
   |     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

error: error number 1
  --> $DIR/03-expand-four-errors.rs:20:5
   |
20 |     compile_error!(concat!("error number ", stringify!(N)));
   |     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

error: error number 2
  --> $DIR/03-expand-four-errors.rs:20:5
   |
20 |     compile_error!(concat!("error number ", stringify!(N)));
   |     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

error: error number 3
  --> $DIR/03-expand-four-errors.rs:20:5
   |
20 |     compile_error!(concat!("error number ", stringify!(N)));
   |     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
```

Solution:

```rust
extern crate proc_macro;

use proc_macro::TokenStream;
use syn::parse::{Parse, ParseStream};
use syn::{parse_macro_input, Result, Token};

#[derive(Debug)]
struct SeqMacroInput {
    from: syn::LitInt,
    to: syn::LitInt,
    tt: proc_macro2::TokenStream,
    ident: syn::Ident,
}

impl Parse for SeqMacroInput {
    fn parse(input: ParseStream) -> Result<Self> {
        let ident = syn::Ident::parse(input)?;
        let _in = <Token![in]>::parse(input)?;
        let from = syn::LitInt::parse(input)?;
        let _dots = <Token![..]>::parse(input)?;
        let to = syn::LitInt::parse(input)?;
        let content;
        let _braces = syn::braced!(content in input);
        let tt = proc_macro2::TokenStream::parse(&content)?;
        Ok(SeqMacroInput {
            from,
            to,
            tt,
            ident,
        })
    }
}

impl Into<proc_macro2::TokenStream> for SeqMacroInput {
    fn into(self) -> proc_macro2::TokenStream {
        // fixme ulnwrsap
        (self.from.base10_parse::<u64>().unwrap()..self.to.base10_parse::<u64>().unwrap())
            .map(|i| self.expand(self.tt.clone(), i))
            .collect()
    }
}

impl SeqMacroInput {
    fn expand2(&self, tt: proc_macro2::TokenTree, i: u64) -> proc_macro2::TokenTree {
        match tt {
            proc_macro2::TokenTree::Group(g) => {
                let mut expanded =
                    proc_macro2::Group::new(g.delimiter(), self.expand(g.stream(), i));
                expanded.set_span(g.span());
                proc_macro2::TokenTree::Group(expanded)
            }
            proc_macro2::TokenTree::Ident(ref ident) if ident == &self.ident => {
                let mut lit = proc_macro2::Literal::u64_unsuffixed(i);
                lit.set_span(ident.span());
                lit.into()
            }
            tt => tt,
        }
    }
    fn expand(&self, stream: proc_macro2::TokenStream, i: u64) -> proc_macro2::TokenStream {
        stream.into_iter().map(|tt| self.expand2(tt, i)).collect()
    }
}

#[proc_macro]
pub fn seq(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as SeqMacroInput);
    let output: proc_macro2::TokenStream = input.into();
    output.into()
}
```

### 04 Paste Ident

The task is to support the following notation:

```rust
seq!(N in 1..4 {
    fn f#N () -> u64 {
        N * 2
    }
});
```

Solution:

```rust
impl SeqMacroInput {
    fn expand2(
        &self,
        tt: proc_macro2::TokenTree,
        rest: &mut proc_macro2::token_stream::IntoIter,
        i: u64,
    ) -> proc_macro2::TokenTree {
        match tt {
            proc_macro2::TokenTree::Group(g) => {
                let mut expanded =
                    proc_macro2::Group::new(g.delimiter(), self.expand(g.stream(), i));
                expanded.set_span(g.span());
                proc_macro2::TokenTree::Group(expanded)
            }
            proc_macro2::TokenTree::Ident(ref ident) if ident == &self.ident => {
                let mut lit = proc_macro2::Literal::u64_unsuffixed(i);
                lit.set_span(ident.span());
                lit.into()
            }
            proc_macro2::TokenTree::Ident(mut ident) => {
                // # followed by self.ident at the end of an identifier
                // or # self.ident #
                let mut peek = rest.clone();
                match (peek.next(), peek.next()) {
                    (
                        Some(proc_macro2::TokenTree::Punct(ref punct)),
                        Some(proc_macro2::TokenTree::Ident(ref ident2)),
                    ) if punct.as_char() == '#' && ident2 == &self.ident => {
                        // saw ident # N
                        ident = proc_macro2::Ident::new(&format!("{}{}", ident, i), ident.span());
                        *rest = peek.clone();

                        // consume closing #
                        match peek.next() {
                            Some(proc_macro2::TokenTree::Punct(ref punct))
                                if punct.as_char() == '#' =>
                            {
                                *rest = peek.clone()
                            }
                            _ => {}
                        }
                    }
                    _ => {}
                }
                proc_macro2::TokenTree::Ident(ident)
            }
            tt => tt,
        }
    }
    fn expand(&self, stream: proc_macro2::TokenStream, i: u64) -> proc_macro2::TokenStream {
        let mut out = proc_macro2::TokenStream::new();
        let mut tts = stream.into_iter();
        while let Some(tt) = tts.next() {
            out.extend(std::iter::once(self.expand2(tt, &mut tts, i)));
        }
        out
    }
}
```

### 05 Repeat Section

The task is to detect repetition groups and support the following:

```rust
use seq::seq;

seq!(N in 0..16 {
    #[derive(Copy, Clone, PartialEq, Debug)]
    enum Interrupt {
        #(
            Irq#N,
        )*
    }
});

fn main() {
    let interrupt = Interrupt::Irq8;

    assert_eq!(interrupt as u8, 8);
    assert_eq!(interrupt, Interrupt::Irq8);
}
```

Code at this point

```rust
extern crate proc_macro;

use proc_macro::TokenStream;
use syn::parse::{Parse, ParseStream};
use syn::{parse_macro_input, Result, Token};

#[derive(Debug)]
struct SeqMacroInput {
    from: syn::LitInt,
    to: syn::LitInt,
    tt: proc_macro2::TokenStream,
    ident: syn::Ident,
}

impl Parse for SeqMacroInput {
    fn parse(input: ParseStream) -> Result<Self> {
        let ident = syn::Ident::parse(input)?;
        let _in = <Token![in]>::parse(input)?;
        let from = syn::LitInt::parse(input)?;
        let _dots = <Token![..]>::parse(input)?;
        let to = syn::LitInt::parse(input)?;
        let content;
        let _braces = syn::braced!(content in input);
        let tt = proc_macro2::TokenStream::parse(&content)?;

        Ok(SeqMacroInput {
            from,
            to,
            tt,
            ident,
        })
    }
}

impl Into<proc_macro2::TokenStream> for SeqMacroInput {
    fn into(self) -> proc_macro2::TokenStream {
        self.expand(self.tt.clone())
    }
}

#[derive(Copy, Clone, Debug)]
enum Mode {
    ReplaceIdent(u64),
    ReplaceSequence,
}

impl SeqMacroInput {
    fn expand2(
        &self,
        tt: proc_macro2::TokenTree,
        rest: &mut proc_macro2::token_stream::IntoIter,
        mutated: &mut bool,
        mode: Mode,
    ) -> proc_macro2::TokenStream {
        let tt = match tt {
            proc_macro2::TokenTree::Group(g) => {
                let (expanded, g_mutated) = self.expand_pass(g.stream(), mode);
                let mut expanded = proc_macro2::Group::new(g.delimiter(), expanded);
                *mutated |= g_mutated;
                expanded.set_span(g.span());
                proc_macro2::TokenTree::Group(expanded)
            }
            proc_macro2::TokenTree::Ident(ref ident) if ident == &self.ident => {
                if let Mode::ReplaceIdent(i) = mode {
                    let mut lit = proc_macro2::Literal::u64_unsuffixed(i);
                    lit.set_span(ident.span());
                    *mutated = true;
                    lit.into()
                } else {
                    // not allowed to replace idents in first pass
                    proc_macro2::TokenTree::Ident(ident.clone())
                }
            }
            proc_macro2::TokenTree::Ident(mut ident) => {
                // # followed by self.ident at the end of an identifier
                // or # self.ident #
                let mut peek = rest.clone();
                match (mode, peek.next(), peek.next()) {
                    (
                        Mode::ReplaceIdent(i),
                        Some(proc_macro2::TokenTree::Punct(ref punct)),
                        Some(proc_macro2::TokenTree::Ident(ref ident2)),
                    ) if punct.as_char() == '#' && ident2 == &self.ident => {
                        // saw ident # N
                        ident = proc_macro2::Ident::new(&format!("{}{}", ident, i), ident.span());
                        *rest = peek.clone();

                        // consume closing #
                        match peek.next() {
                            Some(proc_macro2::TokenTree::Punct(ref punct))
                                if punct.as_char() == '#' =>
                            {
                                *rest = peek.clone()
                            }
                            _ => {}
                        }
                    }
                    _ => {}
                }

                proc_macro2::TokenTree::Ident(ident)
            }
            proc_macro2::TokenTree::Punct(p) if p.as_char() == '#' => {
                if let Mode::ReplaceSequence = mode {
                    // is this a #(...)* ?
                    let mut peek = rest.clone();
                    match (peek.next(), peek.next()) {
                        (
                            Some(proc_macro2::TokenTree::Group(ref rep)),
                            Some(proc_macro2::TokenTree::Punct(ref star)),
                        ) if rep.delimiter() == proc_macro2::Delimiter::Parenthesis
                            && star.as_char() == '*' =>
                        {
                            // expand ... for each sequence in the range
                            *mutated = true;
                            *rest = peek;
                            return (self.from.base10_parse::<u64>().unwrap()
                                ..self.to.base10_parse::<u64>().unwrap())
                                .map(|i| self.expand_pass(rep.stream(), Mode::ReplaceIdent(i)))
                                .map(|(ts, _)| ts)
                                .collect();
                        }
                        _ => {}
                    }
                }
                proc_macro2::TokenTree::Punct(p)
            }
            tt => tt,
        };

        std::iter::once(tt).collect()
    }

    fn expand_pass(
        &self,
        stream: proc_macro2::TokenStream,
        mode: Mode,
    ) -> (proc_macro2::TokenStream, bool) {
        let mut out = proc_macro2::TokenStream::new();
        let mut mutated = false;
        let mut tts = stream.into_iter();
        while let Some(tt) = tts.next() {
            out.extend(self.expand2(tt, &mut tts, &mut mutated, mode));
        }
        (out, mutated)
    }
    fn expand(&self, stream: proc_macro2::TokenStream) -> proc_macro2::TokenStream {
        let (out, mutated) = self.expand_pass(stream.clone(), Mode::ReplaceSequence);
        if mutated {
            return out;
        }

        (self.from.base10_parse::<u64>().unwrap()..self.to.base10_parse::<u64>().unwrap())
            .map(|i| self.expand_pass(stream.clone(), Mode::ReplaceIdent(i)))
            .map(|(ts, _)| ts)
            .collect()
    }
}

#[proc_macro]
pub fn seq(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as SeqMacroInput);
    let output: proc_macro2::TokenStream = input.into();
    output.into()
}
```

### 06 Init Array

Freebie

### 07 Inclusive Range

The task is to allow `N..=M` syntax for the range.

First we must conditionally parse the token:

```rust
fn parse(input: ParseStream) -> Result<Self> {
    let ident = syn::Ident::parse(input)?;
    let _in = <Token![in]>::parse(input)?;
    let from = syn::LitInt::parse(input)?;
    let inclusive = if input.peek(Token![..=]) {
        <Token![..=]>::parse(input)?;
        true
    } else {
        <Token![..]>::parse(input)?;
        false
    };
    let to = syn::LitInt::parse(input)?;
    let content;
    let _braces = syn::braced!(content in input);
    let tt = proc_macro2::TokenStream::parse(&content)?;

    Ok(SeqMacroInput {
        from,
        to,
        inclusive,
        tt,
        ident,
    })
}
```

Then we need to construct the inclusive/exclusive range:

```rust
fn range(&self) -> impl Iterator<Item = u64> {
    let start = self.from.base10_parse::<u64>().unwrap();
    let end = self.to.base10_parse::<u64>().unwrap();

    if self.inclusive {
        start..(end + 1)
    } else {
        start..end
    }
}
```

This can then be used in the place of `self.from..self.to`

### 08 Ident Span

### 09 Interaction With Macrorules

Freebies

## `sorted!`

A linting macro which complains if enums or match branches are not in alphabetical order.

### 01 Parse Enum

The task is to allow the following code to compile:

```rust
use sorted::sorted;

#[sorted]
pub enum Conference {
    RustBeltRust,
    RustConf,
    RustFest,
    RustLatam,
    RustRush,
}

fn main() {}
```

Solution:

```rust
extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::parse_macro_input;

#[proc_macro_attribute]
pub fn sorted(args: TokenStream, input: TokenStream) -> TokenStream {
    assert!(args.is_empty());
    let ty = parse_macro_input!(input as syn::ItemEnum);

    let ts = quote! {#ty};
    ts.into()
}
```

### 02 Not Enum

The task is to provide a specific compiler error message in the following code:

```rust
use sorted::sorted;

#[sorted]
pub struct Error {
    kind: ErrorKind,
    message: String,
}

enum ErrorKind {
    Io,
    Syntax,
    Eof,
}

fn main() {}
```

Solution:

```rust
#[proc_macro_attribute]
pub fn sorted(args: TokenStream, input: TokenStream) -> TokenStream {
    assert!(args.is_empty());
    let input = parse_macro_input!(input as syn::Item);

    sorted_impl(input).into()
}

fn sorted_impl(input: syn::Item) -> proc_macro2::TokenStream {
    if let syn::Item::Enum(e) = input {
        quote! {#e}
    } else {
        quote! {compile_error!("expected enum or match expression");}
    }
}
```

### 03 Out of Order

The task is to show a compiler error indicating that the item `SomethingFailed`
should come before `ThisFailed`:

```rust
use sorted::sorted;

#[sorted]
pub enum Error {
    ThatFailed,
    ThisFailed,
    SomethingFailed,
    WhoKnowsWhatFailed,
}

fn main() {}
```

Solution:

```rust
use syn::spanned::Spanned;

#[proc_macro_attribute]
pub fn sorted(args: TokenStream, input: TokenStream) -> TokenStream {
    assert!(args.is_empty());
    let input = parse_macro_input!(input as syn::Item);

    sorted_impl(input)
        .unwrap_or_else(|e| e.to_compile_error())
        .into()
}

fn sorted_impl(input: syn::Item) -> Result<proc_macro2::TokenStream, syn::Error> {
    if let syn::Item::Enum(e) = input {
        let mut names = Vec::new();
        for variant in &e.variants {
            let name = variant.ident.to_string();
            if names.last().map(|last| &name < last).unwrap_or(false) {
                let next_lex_i = names.binary_search(&name).unwrap_err();
                return Err(syn::Error::new(
                    variant.span(),
                    format!("{} should sort before {}", name, names[next_lex_i]),
                ));
            }
            names.push(name);
        }
        Ok(quote! {#e})
    } else {
        Err(syn::Error::new(
            proc_macro2::Span::call_site(),
            "expected enum or match expression",
        ))
    }
}
```

### 04 Variants with Data

The task is to only get the out of order error from the following code:

```rust
use sorted::sorted;

use std::env::VarError;
use std::error::Error as StdError;
use std::fmt;
use std::io;
use std::str::Utf8Error;

#[sorted]
pub enum Error {
    Fmt(fmt::Error),
    Io(io::Error),
    Utf8(Utf8Error),
    Var(VarError),
    Dyn(Box<dyn StdError>),
}

fn main() {}
```

Note that there will be warnings if we don't include the enum in the macro output as
some of the imports are not used.

Solution:

```rust
extern crate proc_macro;

use proc_macro::TokenStream;
use syn::parse_macro_input;

#[proc_macro_attribute]
pub fn sorted(args: TokenStream, input: TokenStream) -> TokenStream {
    let mut out = input.clone();

    assert!(args.is_empty());
    let ty = parse_macro_input!(input as syn::Item);

    if let Err(e) = sorted_impl(ty) {
        out.extend(TokenStream::from(e.to_compile_error()));
    }
    out
}

fn sorted_impl(input: syn::Item) -> Result<(), syn::Error> {
    if let syn::Item::Enum(e) = input {
        let mut names = Vec::new();
        for variant in &e.variants {
            let name = variant.ident.to_string();
            if names.last().map(|last| &name < last).unwrap_or(false) {
                let next_lex_i = names.binary_search(&name).unwrap_err();
                return Err(syn::Error::new(
                    variant.ident.span(),
                    format!("{} should sort before {}", name, names[next_lex_i]),
                ));
            }
            names.push(name);
        }
        Ok(())
    } else {
        Err(syn::Error::new(
            proc_macro2::Span::call_site(),
            "expected enum or match expression",
        ))
    }
}
```

### 05 Match Expr

The task is to check if match expression branches are sorted:

```rust
use sorted::sorted;

use std::fmt::{self, Display};
use std::io;

#[sorted]
pub enum Error {
    Fmt(fmt::Error),
    Io(io::Error),
}

impl Display for Error {
    #[sorted::check]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Error::*;

        #[sorted]
        match self {
            Io(e) => write!(f, "{}", e),
            Fmt(e) => write!(f, "{}", e),
        }
    }
}

fn main() {}
```

Solution:

```rust
extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::parse_macro_input;
use syn::spanned::Spanned;
use syn::visit_mut::VisitMut;

#[proc_macro_attribute]
pub fn sorted(args: TokenStream, input: TokenStream) -> TokenStream {
    let mut out = input.clone();

    assert!(args.is_empty());
    let ty = parse_macro_input!(input as syn::Item);

    if let Err(e) = sorted_variants(ty) {
        out.extend(TokenStream::from(e.to_compile_error()));
    }
    out
}

fn sorted_variants(input: syn::Item) -> Result<(), syn::Error> {
    if let syn::Item::Enum(e) = input {
        let mut names = Vec::new();
        for variant in &e.variants {
            let name = variant.ident.to_string();
            if names.last().map(|last| &name < last).unwrap_or(false) {
                let next_lex_i = names.binary_search(&name).unwrap_err();
                return Err(syn::Error::new(
                    variant.ident.span(),
                    format!("{} should sort before {}", name, names[next_lex_i]),
                ));
            }
            names.push(name);
        }
        Ok(())
    } else {
        Err(syn::Error::new(
            proc_macro2::Span::call_site(),
            "expected enum or match expression",
        ))
    }
}

#[derive(Default)]
struct LexiographicMatching {
    pub errors: Vec<syn::Error>,
}

impl syn::visit_mut::VisitMut for LexiographicMatching {
    fn visit_expr_match_mut(&mut self, m: &mut syn::ExprMatch) {
        if m.attrs.iter().any(|a| a.path.is_ident("sorted")) {
            // remove #[sorted]
            m.attrs.retain(|a| !a.path.is_ident("sorted"));
            // check the variants
            let mut names = Vec::new();
            for arm in m.arms.iter() {
                let path = get_arm_path(&arm.pat).unwrap();
                let name = path_as_string(path);
                if names.last().map(|last| &name < last).unwrap_or(false) {
                    let next_lex_i = names.binary_search(&name).unwrap_err();
                    self.errors.push(syn::Error::new(
                        path.span(),
                        format!("{} should sort before {}", name, names[next_lex_i]),
                    ));
                }
                names.push(name);
            }
        }

        // keep recursing
        syn::visit_mut::visit_expr_match_mut(self, m)
    }
}

fn path_as_string(path: &syn::Path) -> String {
    path.segments
        .iter()
        .map(|s| format!("{}", quote! {#s}))
        .collect::<Vec<_>>()
        .join("::")
}

fn get_arm_path(arm: &syn::Pat) -> Option<&syn::Path> {
    match *arm {
        syn::Pat::Ident(syn::PatIdent {
            subpat: Some((_, ref sp)),
            ..
        }) => get_arm_path(sp),
        syn::Pat::Path(ref p) => Some(&p.path),
        syn::Pat::Struct(ref s) => Some(&s.path),
        syn::Pat::TupleStruct(ref s) => Some(&s.path),
        _ => None,
    }
}

#[proc_macro_attribute]
pub fn check(args: TokenStream, input: TokenStream) -> TokenStream {
    let mut f = parse_macro_input!(input as syn::ItemFn);
    assert!(args.is_empty());

    let mut lm = LexiographicMatching::default();
    lm.visit_item_fn_mut(&mut f);
    let mut ts = quote! {#f};
    ts.extend(lm.errors.into_iter().map(|e| e.to_compile_error()));
    ts.into()
}
```

Note: it is essential to remove the macro attribute from the source code in
`visit_expr_match_mut` with:

```rust
m.attrs.retain(|a| !a.path.is_ident("sorted"));
```

and not

```rust
m.attrs.retain(|a| a.path.is_ident("sorted"));
```

otherwise it takes you an hour to find what's wrong as the compiler gives the
following error:

```rust
error[E0658]: attributes on expressions are experimental
  --> main.rs:30:9
   |
30 |         #[sorted]
   |         ^^^^^^^^^
   |
   = note: see issue #15701 <https://github.com/rust-lang/rust/issues/15701> for more information
   = help: add `#![feature(stmt_expr_attributes)]` to the crate attributes to enable
error[E0658]: custom attributes cannot be applied to expressions
  --> main.rs:30:9
   |
30 |         #[sorted]
   |         ^^^^^^^^^
   |
   = note: see issue #54727 <https://github.com/rust-lang/rust/issues/54727> for more information
   = help: add `#![feature(proc_macro_hygiene)]` to the crate attributes to enable
error: expected one of: `fn`, `extern`, `use`, `static`, `const`, `unsafe`, `mod`, `type`, `existential`,
 `struct`, `enum`, `union`, `trait`, `auto`, `impl`, `default`, `macro`, identifier, `self`, `super`, `cr
ate`, `::`
  --> main.rs:31:9
   |
31 |         match self {
   |         ^^^^^
error: Fmt should sort before Io
  --> main.rs:33:13
   |
33 |             Fmt(e) => write!(f, "{}", e),
   |             ^^^
warning: unused import: `self::Error::*`
  --> main.rs:28:13
   |
28 |         use self::Error::*;
   |             ^^^^^^^^^^^^^^
   |
   = note: `#[warn(unused_imports)]` on by default
For more information about this error, try `rustc --explain E0658`.
error: could not compile `proc-macro-workshop`
To learn more, run the command again with --verbose.
```
