#[macro_export]
macro_rules! myvec {
    ($($element:expr),*) => {{
        // asserts that count is known at compile time
        const _: usize = $crate::count![@COUNT; $($element),*];

        #[allow(unused_mut)]
        let mut vs = Vec::with_capacity($crate::count![@COUNT; $($element),*]);
        $(
            vs.push($element);
        )*
        vs
    }};
    ($($element:expr,)*) => {{
        $crate::myvec![$($element),*]
    }};
    ($element:expr; $count:expr) => {{
        let count = $count;
        let mut vs = Vec::with_capacity(count);
        vs.resize(count, $element);
        vs
    }};
}

#[macro_export]
#[doc(hidden)]
macro_rules! count {
    (@COUNT; $($element:expr),*) => {
        <[()]>::len(&[$($crate::count![@SUBST; $element]),*])
    };
    (@SUBST; $element:expr) => { () };
}

trait MaxValue {
    fn max_value() -> Self;
}

macro_rules! max_impl {
    ($t:ty) => {
        impl $crate::MaxValue for $t {
            fn max_value() -> Self {
                <$t>::MAX
            }
        }
    };
}

max_impl!(i32);
max_impl!(u32);
max_impl!(i64);
max_impl!(u64);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty() {
        let x: Vec<u32> = myvec![];
        assert!(x.is_empty());
    }

    #[test]
    fn single() {
        let x: Vec<u32> = myvec![42];
        assert!(!x.is_empty());
        assert_eq!(x.len(), 1);
        assert_eq!(x[0], 42);
    }

    #[test]
    fn double() {
        let x: Vec<u32> = myvec![42, 43];
        assert!(!x.is_empty());
        assert_eq!(x.len(), 2);
        assert_eq!(x[0], 42);
        assert_eq!(x[1], 43);
    }

    #[test]
    fn trailing() {
        let x: Vec<u32> = myvec![1,];
        assert!(!x.is_empty());
        assert_eq!(x.len(), 1);
        assert_eq!(x[0], 1);
    }

    #[test]
    fn clone() {
        let x: Vec<u32> = myvec![42; 2];
        assert!(!x.is_empty());
        assert_eq!(x.len(), 2);
        assert_eq!(x[0], 42);
        assert_eq!(x[0], 42);
    }

    #[test]
    fn clone_nonliteral() {
        let mut y = Some(42);
        let x: Vec<u32> = myvec![y.take().unwrap(); 2];
        assert!(!x.is_empty());
        assert_eq!(x.len(), 2);
        assert_eq!(x[0], 42);
        assert_eq!(x[0], 42);
    }

    #[test]
    fn efficient_count() {
        let x: Vec<u32> = myvec![42];
        println!("Capacity of myvec![42] = {}", x.capacity());
        let y: Vec<u32> = myvec![42; 739];
        assert_eq!(y.capacity(), 739);
    }
}

/// ```compile_fail
/// let x: Vec<u32> = macros::myvec![42; "foo"];
/// ```
/// ```compile_fail
/// struct Foo;
/// let y = Foo;
/// let x: Vec<Foo> = macros::myvec![Foo; 5];
/// ```
#[allow(dead_code)]
struct CompileFailTest;
