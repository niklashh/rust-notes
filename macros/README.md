# Macros

Video URL: https://www.youtube.com/watch?v=q6paRBbLgNw

## `vec` macro

https://doc.rust-lang.org/std/macro.vec.html

You define a new macro using the `macro_rules!` macro. Then inside the macro you
write patterns or arguments which describe the syntax you use when invoking the
macro.

## Excellent reference for macros

https://danielkeep.github.io/tlborm/book/index.html

## Macro syntax

Macros can be invoked using different delimiters.

```rust
macro_rules! myvec {
    () => {};
}

myvec!();
myvec![];
myvec!{}
```

Macros can have nearly arbitrary syntax for arguments.

```rust
macro_rules! myvec {
    ($arg1:ty, $arg2:expr, $arg3:path) => {}; // this is fine
    ($arg1:ty => $arg2:expr; $arg3:path) => {}; // this is also ok
}

myvec!{
    u32 => x.foo(); std::path
}
```

The syntax doesn't have to be valid rust, but has to be valid grammar. The
output has to be valid rust.

> You can install `cargo-expand` which will expand all the macros in the current
> crate

```rust
macro_rules! myvec {
    ($arg1:ty => $arg2:ident) => {
        type $arg2 = $arg1;
    };
}

myvec!{
    u32 => alsou32
}

const n: alsou32 = 1;
```

```rust
$ cargo expand
#![feature(prelude_import)]
#[prelude_import]
use std::prelude::v1::*;
#[macro_use]
extern crate std;
type alsou32 = u32;
const n: alsou32 = 1;
```

Variables (but not types) exist in another world so you cannot create do the
following

```rust
macro_rules! myvec {
    () => {
        let x = 42;
    };
}

fn foo() {
    myvec!();
    x == 1;
}
```

Same in the other direction

```rust
macro_rules! myvec {
    () => {
        x += 42;
    };
}

fn foo() {
    let mut x = 1;
    myvec!();
}
```

This is one of the things that make rust macros hygienic.

This is what you would need if you wanted to use the variable inside the macro

```rust
macro_rules! myvec {
    ($x:ident) => {
        $x += 42;
    };
}

fn foo() {
    let mut x = 1;
    myvec!(x);
}
```

> Notice that you are not passing ownership to the macro, only the name of the
> variable

## The empty vector

```rust
#[macro_export]
macro_rules! myvec {
    () => {
        Vec::new()
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty() {
        let x: Vec<u32> = myvec![];
        assert!(x.is_empty());
    }
}
```

Expanding the file with tests

```shell
cargo expand --lib --tests
```

```rust
#![feature(prelude_import)]
#[prelude_import]
use std::prelude::v1::*;
#[macro_use]
extern crate std;
#[cfg(test)]
mod tests {
    use super::*;
    extern crate test;
    #[cfg(test)]
    #[rustc_test_marker]
    pub const empty: test::TestDescAndFn = test::TestDescAndFn {
        desc: test::TestDesc {
            name: test::StaticTestName("tests::empty"),
            ignore: false,
            allow_fail: false,
            should_panic: test::ShouldPanic::No,
            test_type: test::TestType::UnitTest,
        },
        testfn: test::StaticTestFn(|| test::assert_test_result(empty())),
    };
    fn empty() {
        let x: Vec<u32> = Vec::new();
        if !x.is_empty() {
            {
                ::std::rt::begin_panic("assertion failed: x.is_empty()")
            }
        };
    }
}
#[main]
pub fn main() -> () {
    extern crate test;
    test::test_main_static(&[&empty])
}
```

## Non-empty vectors

Test driven test

```rust
#[test]
fn single() {
    let x: Vec<u32> = myvec![42];
    assert!(!x.is_empty());
    assert_eq!(x.len(), 1);
    assert_eq!(x[0], 42);
}
```

What we want to add to our macro is a new pattern which matches an expression
(almost anything/anything that you could terminate with a `;`).

```rust
($element:expr) => {
    let mut vs = Vec::new();
    vs.push($element);
    vs
};
```

The issue with this is that macros are not functions and using let inside them
is not supported because it expands to this

```rust
let x : Vec < u32 > = let mut vs = Vec :: new () ;
```

What we want to do is to return a code block which returns the vector

```rust
($element:expr) => {{
    let mut vs = Vec::new();
    vs.push($element);
    vs
}};
```

And the test expands to

```rust
let x: Vec<u32> = {
    let mut vs = Vec::new();
    vs.push(42);
    vs
};
```

You can choose which delimiter to use in the output of the macro just like in
the invocation. But rust format changes it back to braces.

```rust
($element:expr) => [{
    let mut vs = Vec::new();
    vs.push($element);
    vs
}];
```

## Declarative vs procedural macros

Proc macros allow you to write a rust program, but declaratives macros only
allow you to write substitutions.

Declarative macros don't have access to type information.

Proc macros aren't constrained by the same syntax requirements as declarative
macros.

Proc macros take in a stream of rust tokens and return a stream of rust tokens.

## Repeated macro arguments

```rust
#[test]
fn double() {
    let x: Vec<u32> = myvec![42, 43];
    assert!(!x.is_empty());
    assert_eq!(x.len(), 2);
    assert_eq!(x[0], 42);
    assert_eq!(x[1], 43);
}
```

we want our `myvec` macro to allow an arbitrary number of "arguments"

```rust
($($element:expr),+) => {{
```

This says we have one or more comma separated things.

Inside the macro we can use the same syntax `$()` to expand the repetition

```rust
($($element:expr),+) => {{
    let mut vs = Vec::new();
    $(
        vs.push($element);
    )+
    vs
}}
```

If the argument contains multiple repeated arguments for different variables
the compiler detects which meta-variable to repeat by looking into the
repetition inside the macro.

If you use variables from multiple patterns inside the repetition, they need to
repeat the same number of times.

## Trailing comma

Often we would want to be able to have a comma after the repeated expressions,
like in

```rust
myvec![
    1,
    2,
    3,
]
```

To do this, we add a pattern which allows a commas

```rust
($($element:expr),+ $(,)?) => {{
```

Regex `{N,M}` is not supported. Only `*`, `+` and `?` are.

## Why are macros useful

Example

```rust
trait MaxValue {
    fn max_value() -> Self;
}

macro_rules! max_impl {
    ($t:ty) => {
        impl $crate::MaxValue for $t {
            fn max_value() -> Self {
                <$t>::MAX
            }
        }
    }
}

max_impl!(i32);
max_impl!(u32);
max_impl!(i64);
max_impl!(u64);
```

## Vector by repetition

```rust
($element:expr; $count:expr) => {{
    let mut vs = Vec::new();
    for _ in 0..$count {
        vs.push($element);
    }
    vs
}}
```

However this implementation repeats the element expression which is undesired.

A failing test

```rust
#[test]
fn clone_nonliteral() {
    let mut y = Some(42);
    let x: Vec<u32> = myvec![y.take().unwrap(); 2];
    assert!(!x.is_empty());
    assert_eq!(x.len(), 2);
    assert_eq!(x[0], 42);
    assert_eq!(x[0], 42);
}
```

Fix

```rust
let mut vs = Vec::new();
let x = $element;
for _ in 0..$count {
    vs.push(x.clone());
}
vs
```

## Macro rules readability

Declarative macros are unreadable when they become large. At that point a proc
macro would be better, but it adds dependencies and increases build time.

## Test something that doesn't compile

A trict to ensure something doesn't compile is to use doc comments

````rust
/// ```compile_fail
/// let x: Vec<u32> = macros::myvec![42; "foo"];
/// ```
#[allow(dead_code)]
struct CompileFailTest;
````

Now when running tests, it runs the doc test as well

```
   Doc-tests macros

running 1 test
test src/lib.rs - CompileFailTest (line 102) ... ok
```

## Tidying up the patterns

We can get rid of the empty pattern in the macro by allowing 0 occurrences

```rust
($($element:expr),* $(,)?) => {{
    let mut vs = Vec::new();
    $(
        vs.push($element);
    )*
    vs
}};
```

This however leads to a linting warning because of the case where we never push

```rust
#[allow(unused_mut)]
```

Think of `$[macro_export]` as `pub`

## Reallocations for repetition constructor

When we create a new vector with the pattern `[42; 100]`, our macro first
creates a `Vec` with some default capacity. But as the macro is pushing new
elements in to the vector it needs to increase the vector's capacity and
therefor reallocate memory.

We can also get rid of the push with `extend`

```rust
($element:expr; $count:expr) => {{
    let count = $count;
    let mut vs = Vec::with_capacity(count);
    vs.extend(std::iter::repeat($element).take(count));
    vs
}}
```

## Macro argument trait bounds

The following code won't compile because `Foo` isn't clone, but repeat expects
it to be clone

```rust
struct Foo;
let y = Foo;
let x: Vec<Foo> = myvec![Foo; 5];
```

## `use` hygiene in macros

It is possible to override the modules the macro uses. To avoid that, the macro
can use `$crate` to access exportend items or `::mod` to specify an absolute
path.

Therefore it would be better to use `::std` in case the the user is using their
own std module.

```rust
vs.extend(::std::iter::repeat($element).take(count));
```

Or just don't use `std::iter`

```rust
vs.resize(count, $element);
```

## The standard library pattern

Our version of vec allows a gratituous comma

```rust
myvec![,]; // valid
```

Which can be solved by using two patterns instead. Copy-pasting the code is
unnecessary in this case

```rust
($($element:expr),*) => {{
    #[allow(unused_mut)]
    let mut vs = Vec::new();
    $(
        vs.push($element);
    )*
    vs
}};
($($element:expr,)*) => {{
    $crate::myvec![$($element),*]
}};
```

## The need for counting

To get around vec resizing, we need to count the number of arguments matched by
the macro pattern

```rust
let count = [$($element),*].len();
```

This however puts the element into multiple pieces in the code

Note that we don't need the `$element` inside the macro to get the length of the
array, it could be units instead

```rust
[$(()),*].len()
```

But now the compiler doesn't know how many times it needs to repeat the block
because `$element` isn't inside it

```rust
(@COUNT; $($element:expr),*) => {
    [$($crate::myvec![@SUBST; $element]),*].len()
};
(@SUBST; $element:expr) => { () };
```

Now the compiler is saying that it cannot infer the type of the array

```rust
<[()]>::len(&[$($crate::myvec![@SUBST; $element]),*])
```

This calls the implematation for len for slices (not array) of unit. Because
arrays implement AsRef slice we are allowed to call slice methods on an array.
The slice length is also known at compile time so it doesn't cost anything!

The double test expands to

```rust
let x: Vec<u32> = {
    #[allow(unused_mut)]
    let mut vs = Vec::with_capacity(<[()]>::len(&[(), ()]));
    vs.push(42);
    vs.push(43);
    vs
};
```

[Counting in the macro
book](https://danielkeep.github.io/tlborm/book/blk-counting.html)

## Ensuring count is computed at compile time

```rust
// asserts that count is known at compile time
const _: usize = $crate::myvec![@COUNT; $($element),*];
```

## Hiding internal macro patterns

```rust
#[macro_export]
#[doc(hidden)]
macro_rules! count {
    (@COUNT; $($element:expr),*) => {
        <[()]>::len(&[$($crate::count![@SUBST; $element]),*])
    };
    (@SUBST; $element:expr) => { () };
}
```

To view the doc, run

```shell
cargo doc --open
```

<!-- vim: set sw=4 ts=4 tw=80 : -->
