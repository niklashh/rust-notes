#![feature(dropck_eyepatch)]

use std::iter::Empty;
use std::marker::PhantomData;
use std::ptr::NonNull;

pub struct Boks<T> {
    p: NonNull<T>,
    // The PhantomData tells the compiler that the Drop may
    // drop the T
    _t: PhantomData<T>,
    // _t: PhantomData<fn() -> T>
    // makes Boks covariant over T but then the drop check is disabled
}

struct EmptyIterator<T> {
    _t: PhantomData<T>, // <fn() -> T>,
}

impl<T> Default for EmptyIterator<T> {
    fn default() -> Self {
        Self { _t: PhantomData }
    }
}

impl<T> Iterator for EmptyIterator<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        None
    }
}

// The `may_dangle` tells the compiler that the Drop doesn't
// access the T
unsafe impl<#[may_dangle] T> Drop for Boks<T> {
    fn drop(&mut self) {
        // let _: u8 = unsafe { std::ptr::read(self.p as *const u8) };
        // SAFETY: p was constructed from a Box in the first place
        // and hasn't been freed as self exists
        unsafe { Box::from_raw(self.p.as_mut()) };
    }
}

impl<T> Boks<T> {
    pub fn ny(t: T) -> Self {
        Boks {
            // SAFETY: Box never creates a null pointer
            p: unsafe { NonNull::new_unchecked(Box::into_raw(Box::new(t))) },
            _t: PhantomData,
        }
    }
}

impl<T> std::ops::Deref for Boks<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        // SAFETY: is valid since it was constructed from a valid T
        // and turned into a pointer through Box which creates aligned
        // pointers, and the data hasn't been freed because self is alive
        unsafe { &*self.p.as_ref() }
    }
}
impl<T> std::ops::DerefMut for Boks<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        // SAFETY: is valid since it was constructed from a valid T
        // and turned into a pointer through Box which creates aligned
        // pointers, and the data hasn't been freed because self is alive
        // Also as we have &mut self, no one else has access to p
        unsafe { &mut *self.p.as_mut() }
    }
}

use core::fmt::Debug;

#[derive(Debug)]
struct Oisann<'a>(&'a mut u32);

impl<'a> Drop for Oisann<'a> {
    fn drop(&mut self) {
        *self.0 += 1;
        println!("dropped {:?}", self);
    }
}

fn main() {
    let x = 42;
    let b = Boks::ny(x);
    println!("{:?}", *b);

    let mut y = 42;
    let b = Boks::ny(&mut y);
    println!("{:?}", y);

    let mut z = 42;
    let b = Boks::ny(Oisann(&mut z));
    drop(b);
    println!("{:?}", z);

    let s = String::from("hei");
    let mut boks1 = Boks::ny(&*s);
    let boks2: Boks<&'static str> = Boks::ny("heisann");
    boks1 = boks2;

    let mut a = 32;
    let mut it = EmptyIterator::default();
    let mut o = Some(Oisann(&mut a));
    o = it.next();
    drop(o);
    println!("{:?}", a);
}
