#[derive(Clone, Default, Debug)]
pub struct Mount {
    pub device: String,
    pub mount_point: String,
    pub file_system_type: String,
    pub options: Vec<String>,
}

pub(self) mod parsers {
    use nom::{
        branch, bytes,
        character::{self, complete::space1},
        combinator, multi, sequence, IResult,
    };

    use super::Mount;

    fn not_whitespace(i: &str) -> IResult<&str, &str> {
        bytes::complete::is_not(" \t")(i)
    }

    fn escaped_space(i: &str) -> IResult<&str, &str> {
        combinator::value(" ", bytes::complete::tag("040"))(i)
    }

    fn escaped_backslash(i: &str) -> IResult<&str, &str> {
        combinator::recognize(character::complete::char('\\'))(i)
    }

    fn transform_escaped(i: &str) -> IResult<&str, String> {
        bytes::complete::escaped_transform(
            bytes::complete::is_not("\\"),
            '\\',
            branch::alt((escaped_backslash, escaped_space)),
        )(i)
    }

    fn mount_opts(i: &str) -> IResult<&str, Vec<String>> {
        multi::separated_list1(
            character::complete::char(','),
            combinator::map_parser(bytes::complete::is_not(", \n"), transform_escaped),
        )(i)
    }

    pub fn parse_line(i: &str) -> IResult<&str, Mount> {
        match combinator::all_consuming(sequence::tuple((
            combinator::map_parser(not_whitespace, transform_escaped), // device
            space1,
            combinator::map_parser(not_whitespace, transform_escaped), // mount_point
            space1,
            not_whitespace, // file_system_type
            space1,
            mount_opts, // options
            space1,
            character::complete::char('0'),
            space1,
            character::complete::char('0'),
            nom::character::complete::space0,
        )))(i)
        {
            Ok((
                remaining_input,
                (device, _, mount_point, _, file_system_type, _, options, _, _, _, _, _),
            )) => Ok((
                remaining_input,
                Mount {
                    device,
                    mount_point,
                    file_system_type: file_system_type.into(),
                    options,
                },
            )),
            Err(e) => Err(e),
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        macro_rules! nom_error {
            ($input:expr, $type:tt) => {
                Err(::nom::Err::Error(::nom::error::Error {
                    input: $input,
                    code: ::nom::error::ErrorKind::$type,
                }))
            };
        }

        #[test]
        fn test_not_whitespace() {
            assert_eq!(not_whitespace("abcd efg"), Ok((" efg", "abcd")));
            assert_eq!(not_whitespace("abcd\tefg"), Ok(("\tefg", "abcd")));
            assert_eq!(not_whitespace(" abcdefg"), nom_error!(" abcdefg", IsNot));
        }

        #[test]
        fn test_escaped_space() {
            assert_eq!(escaped_space("040"), Ok(("", " ")));
            assert_eq!(escaped_space(" "), nom_error!(" ", Tag));
        }

        #[test]
        fn test_escaped_backslash() {
            assert_eq!(escaped_backslash("\\"), Ok(("", "\\")));
            assert_eq!(
                escaped_backslash("not a backslash"),
                nom_error!("not a backslash", Char)
            );
        }

        #[test]
        fn test_transform_escaped() {
            assert_eq!(
                transform_escaped("abc\\040def\\\\g\\040h"),
                Ok(("", std::string::String::from("abc def\\g h")))
            );
            assert_eq!(transform_escaped("\\bad"), nom_error!("bad", Tag));
        }

        #[test]
        fn test_mount_opts() {
            assert_eq!(mount_opts(""), nom_error!("", IsNot));
            assert_eq!(
                mount_opts("a,bc,d\\040e"),
                Ok(("", vec!["a".into(), "bc".into(), "d e".into()]))
            );
        }

        #[test]
        fn test_parse_line() {
            let device = "tmpdevice";
            let mount_point = "/tmp/\\backslash";
            let file_system_type = "ext4";
            let options: Vec<String> =
                vec!["options".into(), "a".into(), "b=c".into(), "d e".into()];

            let (_, mount) =
                parse_line(r#"tmpdevice /tmp/\\backslash    ext4 options,a,b=c,d\040e 0 0 "#)
                    .unwrap();
            assert_eq!(mount.device, device);
            assert_eq!(mount.mount_point, mount_point);
            assert_eq!(mount.file_system_type, file_system_type);
            assert_eq!(mount.options, options);
        }
    }
}
