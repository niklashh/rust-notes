# Iterators

Video url: https://www.youtube.com/watch?v=yozQ9C69pNs&list=PLqbS7AVVErFiWDOAVrPt7aYmnuuOLYvOa&index=3

## The `Iterator` trait

https://doc.rust-lang.org/std/iter/trait.Iterator.html

```rust
pub trait Iterator {
    Type Item; // the type that gets yielded by the iterator

    fn next(&mut self) -> Option<Self::Item>;
}
```

```rust
fn main() {
    for x in vec!["a", "b", "c"] {
        // ...
    }
    // turns into
    let mut iter = vec!["a", "b", "c"].into_iter();
    while let Some(e) = iter.next() {
        // ...
    }
}
```

## The `IntoIterator` trait

https://doc.rust-lang.org/std/iter/trait.IntoIterator.html

Is kind of a wrapper around `Iterator` which says that some a type implementing
it can be turned into an iterator of some kind.

```rust
pub trait IntoIterator {
    type Item;
    type IntoIter: Iterator;
}
```

## Generic traits vs associated types

Why does `Iterator` have an `Item` associated to it rather than a generic?

```rust
// Why not this?
trait Iterator<Item> {
    fn next(&mut self) -> Option<Item>;
}
```

There's no right or wrong way, but in general you use an associated type when
you assume there will be only one implementation of the trait for a given type.

A `HashMap` and a `Vec` are examples of types which can be converted iterated
over in one useful way. Of course a type `Vec<(T, U)>` might have two
iterator implementations for getting all `T`s or `U`s, but you would rather use
an iterator adaptor from the vector's iterator.

```rust
trait Service<Request> {
    fn do(&mut self, r: Request);
}
```

Here it might make sense that a type that implements `Service` accepts multiple
different types of requests by using multiple implementations of the trait. But
if the `Service` trait used an associated type `type Request`, you could not
implement it multiple times.

Generally using an associated type reduces the amount of type parameters you
need to be using which makes the code clear and compiler happier.

```rust
fn main() {
    ["a", "b"].iter() // gives an iterator to the slice
    let vs = vec![1, 2, 3];
    for v in vs {
        // consumes vs, v is owned
    }
    for v in vs.iter() {
        // borrows vs, & to v
    }
}
```

## Provided `Iterator` methods

Iterators have default implementations for methods. (you can detect that it has
a default implementation when there's a `{ ... }` at the end of the
declaration). These default implementations are only allowed to use the `next`
method.

`count` calls `self.next` until it returns `None` and keeps track of how many
items there were. Of course for some other iterators there can be a more
efficient implementation of `count`. For example `Vec` knows it's length so it
can just return that.

`flatten` walks the items of the iterator (which in turn are expected to be able
to be turned into iterators) and it walks their items. Therefore repeatedly
calling `next` on a flattened iterator returns all the items in the inner
iterators in order.

```rust
fn flatten(self) -> Flatten<Self>
where
    Self::Item: IntoIterator {}
```

An example implementation of `flatten` could be

```rust
pub fn flatten<I>(iter: I) -> Flatten<I> {
    Flatten::new(iter)
}

pub struct Flatten<O> {
    outer: O,
}

impl<O> Flatten<O> {
    fn new(iter: O) -> Self {
        Flatten { outer: iter }
    }
}

impl<O> Iterator for Flatten<O>
where
    O: Iterator,
    O::Item: IntoIterator,
{
    type Item = <O::Item as IntoIterator>::Item;
    fn next(&mut self) -> Option<Self::Item> {
        self.outer.next().and_then(|inner| inner.into_iter().next())
    }
}
```

But this has an issue, it doesn't walk over the inner iterator properly.
Therefore the following test fails (count is 1)

```rust
#[test]
fn two() {
    assert_eq!(flatten(std::iter::once(vec!["a", "b"])).count(), 2);
}
```

If we open the `and_then` we can see more cleary what's going on

```rust
fn next(&mut self) -> Option<Self::Item> {
    let inner_item = self.outer.next()?;
    let mut inner_iter = inner_item.into_iter();
    inner_iter.next()
}
```

Here, every time we call next, `next` is called on the outer iterator which
means that it doesn't ever get to the second item in `inner_iter`.

The solution is to store `inner_iter` somewhere

```rust
pub struct Flatten<O>
where
    O: Iterator,
{
    outer: O,
    inner: Option<O::Item>, // an Option<Vec> for example
}

impl<O> Flatten<O>
where
    O: Iterator,
{
    fn new(iter: O) -> Self {
        Flatten {
            outer: iter,
            inner: None,
        }
    }
}

// ...
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(inner_iter) = self.inner {
            if let Some(item) = inner_iter.next() { // no method named `next`
                 return Some(item);                 // found for associated type
            }                                       // <O as Iterator>::Item
            self.inner = None
        }
        let inner_item = self.outer.next()?;
        let mut inner_iter = inner_item.into_iter();
        inner_iter.next()
    }
```

The issue now is that you can't call `next` on a vector because it is not an
iterator, but can be converted into one using `into_iter` from the
`IntoIterator` trait.

Really what the inner needs to be is

```rust
Option<<O::Item as IntoIterator>::IntoIter>
```

The trait bounds need to updated which makes the code a bit more unreadable, but
a fix for that is coming to Rust.

```rust
pub fn flatten<I>(iter: I) -> Flatten<I>
where
    I: Iterator,
    I::Item: IntoIterator,
{
    Flatten::new(iter)
}

pub struct Flatten<O>
where
    O: Iterator,
    O::Item: IntoIterator,
{
    outer: O,
    inner: Option<<O::Item as IntoIterator>::IntoIter>,
}

impl<O> Flatten<O>
where
    O: Iterator,
    O::Item: IntoIterator,
{
    fn new(iter: O) -> Self {
        Flatten {
            outer: iter,
            inner: None,
        }
    }
}
```

## `DoubleEndedIterator`

`DoubleEndedIterator` adds the method `next_back` which allows the programmer to
consume the iterator from the back end too.

Our `Flatten` type could implement `DoubleEndedIterator` when the underlying
iterators implement it too.

We can reference `self::Item` from `Iterator` because `DoubleEndedIterator`
extends the trait (same with `Deref` > `DerefMut`).

Firstly we need to make sure that the inner types implement
`DoubleEndedIterator`

```rust
impl<O> DoubleEndedIterator for Flatten<O>
where
    O: DoubleEndedIterator, // no need to mention Iterator because it is implied
    O::Item: IntoIterator,
    <O::Item as IntoIterator>::IntoIter: DoubleEndedIterator,
{
    fn next_back(&mut self) -> Option<Self::Item> {}
}
```

And the copy paste the `Iterator` implementation while replacing `next` with
`next_back`.

To use this behavior we call `.rev()` on the flattened iterator.

But this fails

```rust
#[test]
fn both_ends() {
    let mut iter = flatten(vec![vec!["a", "b"], vec!["c", "d"]]);
    assert_eq!(iter.next(), Some("a"));
    assert_eq!(iter.next_back(), Some("d"));
    assert_eq!(iter.next(), Some("b"));
    assert_eq!(iter.next_back(), Some("c"));
    assert_eq!(iter.next(), None);
    assert_eq!(iter.next_back(), None);
}
```

Because our type doesn't keep track of `next` and `next_back` separately but
instead uses `inner` for both.

Also the code needs to take into account that `next` stops where `next_back`
starts. This means that when the outer iterator has been exhausted we need to
start walking the back iterator from the front.

Same applies for the back iterator. Walk the front iterator from the back that
is.

Notice that we do not need the back iterator in the struct when the code only
needs `next`. This would require specialization because you would need to have
a different implementation of iterator depending on whether or not you need
`next_back`. Also the compiler doesn't optimize away the field.

However, overhead from the extra iterator (which lies on the stack) is not that
big (compared to memory allocations for example).

## `ref` in patterns

Why does

```rust
if let Some(ref mut front_iter) = self.front_iter
```

need `ref mut`?

```rust
self.front_iter = Option<T>;

if let Some(t /* the type of t is T (owned, not a reference) */)
= self.front_iter {

}
```

Because `t` is not a reference to the data inside the Option `self.front_iter`,
the data needs to _move_ to t. This is however not allowed because we only have
an exclusive reference to `self`: `fn next(&mut self)`.

There are a few ways to not _move_ in this situation

1. calling `as_mut: Option<T> -> Option<&mut T>`

   ```rust
   if let Some(front_iter) = self.front_iter.as_mut()
   ```

1. taking an exclusive reference in the pattern matching with `ref`

   ```rust
   if let Some(ref mut front_iter) = self.front_iter
   ```

1. hinting the compiler to borrow using an exclusive reference

   ```rust
   if let Some(front_iter) = &mut self.front_iter
   ```

## Ergonomics through extension traits

We don't want to have `flatten` be a free standing function, but instead, if you
have an iterator you could call flatten on it. We can do that by extending the
`Iterator` trait

```rust
pub trait IteratorExt: Iterator {
    fn our_flatten(self) -> Flatten<Self>
    where
        Self::Item: IntoIterator,
    {
        flatten(self)
    }
}
```

We can use a blanket implementation to implement `IteratorExt` for all iterators

```rust
impl<T> IteratorExt for T
where
    T: Iterator,
{
    fn our_flatten(self) -> Flatten<Self>
    where
        Self::Item: IntoIterator,
    {
        flatten(self)
    }
}
```

## `Sized` in traits

The reason `IteratorExt` doesn't compile is because `Self` is not `Sized`.

Basically when you have a `Flatten` it stores an `O` inside it, therefore `O`
has to be sized (cannot be a trait for example), because it is stored inside a
struct.

Therefore `Self` has to be sized for our trait to compile

```rust
pub trait IteratorExt: Iterator {
    fn our_flatten(self) -> Flatten<Self>
    where
        Self: Sized,
        Self::Item: IntoIterator,
    {
        flatten(self)
    }
}
```

<!-- vim: set sw=4 ts=4 tw=80 : -->
