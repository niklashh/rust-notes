use std::cell::UnsafeCell;

pub struct Cell<T> {
    value: UnsafeCell<T>,
}

// implied by UnsafeCell
// impl<T> !Sync for Cell<T> {}
// unsafe impl<T> Sync for Cell<T> {}

impl<T> Cell<T> {
    pub fn new(value: T) -> Self {
        Cell {
            value: UnsafeCell::new(value),
        }
    }

    pub fn set(&self, value: T) {
        unsafe { *self.value.get() = value };
    }

    // pub fn get(&self) -> &T {
    pub fn get(&self) -> &T
    where
        T: Copy,
    {
        unsafe { &*self.value.get() }
    }
}

/*
#[cfg(test)]
mod test {
    use super::Cell;

    const SIZE: usize = 100_000;

    #[test]
    fn bad() {
        use std::sync::Arc;
        use std::thread;
        let x = Arc::new(Cell::new(0));

        let x1 = x.clone();
        let t1 = thread::spawn(move || {
            for _ in 0..SIZE {
                let x = x1.get();
                x1.set(x + 1)
            }
        });

        // t1.join().unwrap();
        let x2 = x.clone();
        let t2 = thread::spawn(move || {
            for _ in 0..SIZE {
                let x = x2.get();
                x2.set(x + 1);
            }
        });

        t1.join().unwrap();
        t2.join().unwrap();
        assert_eq!(*x.get(), 2 * SIZE);
    }

    #[test]
    fn bad2() {
        let x = Cell::new(String::from("hello"));
        let first = x.get();
        x.set(String::from("world"));
        assert_eq!(first, "hello");
    }
}
*/
