use core::cell::Cell;
use std::marker::PhantomData;
use std::ptr::NonNull;

struct RcInner<T> {
    value: T,
    refcount: Cell<usize>,
}

pub struct Rc<T> {
    inner: NonNull<RcInner<T>>,
    _marker: PhantomData<RcInner<T>>,
}

impl<T> Rc<T> {
    pub fn new(v: T) -> Self {
        let inner = Box::new(RcInner {
            value: v,
            refcount: Cell::new(1),
        });

        Rc {
            // SAFETY: Box never gives us a null pointer
            inner: unsafe { NonNull::new_unchecked(Box::into_raw(inner)) },
            _marker: PhantomData,
        }
    }
}

impl<T> std::ops::Deref for Rc<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        // SAFETY: self.inner is a Box that is deallocated only when the last Rc is dropped
        // we have an Rc, therefore the Box is still alive and dereferencing it is fine
        &unsafe { self.inner.as_ref() }.value
    }
}

impl<T> Clone for Rc<T> {
    fn clone(&self) -> Self {
        let inner = unsafe { self.inner.as_ref() };
        let c = inner.refcount.get();
        inner.refcount.set(c + 1);
        Rc { inner: self.inner, _marker: PhantomData }
    }
}

impl<T> Drop for Rc<T> {
    fn drop(&mut self) {
        let inner = unsafe { self.inner.as_ref() };
        let c = inner.refcount.get();
        if c == 1 {
            drop(inner);
            // SAFETY: we are the only Rc left, and therefore
            // dropping us means there are no more refences to
            let _ = unsafe { Box::from_raw(self.inner.as_ptr()) };
        // the compiler should stop the following but it doesn't...
        // inner.refcount.set(0)
        } else {
            // SAFETY: there are other Rcs so don't drop the Box
            inner.refcount.set(c - 1);
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn what() {
        let (x, y);
        x = String::from("foo");
        y = Rc::new(&x);
    }
}
