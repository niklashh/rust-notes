# Smart pointers and interior mutability

Video url: https://www.youtube.com/watch?v=8O0Nt9qY_vo

## Interior mutability

`Box` does not provide interior mutability. If you have a shared referenc to
a box you cannot mutate the innards.

The cost/overhead of a smart pointer goes up with the amount of freedom you have
for what you can put inside the pointer.

## `std::cell`

https://doc.rust-lang.org/std/cell/

```text
Shareable mutable containers
```

### `Cell`

`Cell` is usually used for smaller values like numbers or flags that need to be
mutated from different places. Cells are typically used with thread locals, as
you know there's only one thread accessing it.

Note that `cell.get` does not give you a reference, instead it gives a copy

```rust
impl<T> Cell<T> where T: Copy {
    fn get(&self) -> T
}
```

In fact there's no way to get a reference to what's inside a `cell`. Therefore
mutating the value inside it is fine. This is the point of `cell`.

Another thing that is needed for this to be safe is that `cell` doesn't
implement `Sync`:

```rust
impl<T> !Sync for Cell<T>
```

Therefore if you have a reference to a `cell` you cannot give away that
reference to another thread.

With a `cell` you never use an exclusive reference (&mut T).

https://docs.rs/dtolnay/0.0.9/dtolnay/macro._02__reference_types.html

### `UnsafeCell`

`UnsafeCell` gives you a way to get an exclusive pointer the value whenever you
want hence the unsafety. It is a building block to create useful types which the
programmer makes sure are safe.

`UnsafeCell` makes it possible for our cell implementation to get mutable access
to the value from a shared reference.

### Example implementation

```rust
use std::cell::UnsafeCell;

pub struct Cell<T> {
    value: UnsafeCell<T>,
}

impl<T> Cell<T> {
    pub fn new(value: T) -> Self {
        Cell {
            value: UnsafeCell::new(value),
        }
    }
}
```

Now we could use the UnsafeCell's get method to get a pointer to the value and
dereference it

```rust
// ...
pub fn set(&self, value: T) {
    unsafe { *self.value.get() = value };
}
pub fn get(&self) -> T
where
    T: Copy,
{
    unsafe { *self.value.get() }
}
```

Writing `unsafe` there tells the compiler that the programmer has checked that
no other thread is mutating the value. This is a very bad practice.

A test that would cause unwanted behavior

```rust
fn bad() {
    use std::sync::Arc;
    use std::thread;
    let x = Arc::new(Cell::new(0));

    let x1 = x.clone();
    let t1 = thread::spawn(move || {
        for _ in 0..SIZE {
            let x = x1.get();
            x1.set(x + 1)
        }
    });

    // t1.join().unwrap();
    let x2 = x.clone();
    let t2 = thread::spawn(move || {
        for _ in 0..SIZE {
            let x = x2.get();
            x2.set(x + 1);
        }
    });

    t1.join().unwrap();
    t2.join().unwrap();
    assert_eq!(*x.get(), 2 * SIZE);
}
```

To fix the race condition we need to make sure cells don't get shared accross
threads

```rust
impl<T> !Sync for Cell<T> {}
```

But as `UnsafeCell` already implements not sync, it is implied that `Cell`
implements it too.

Running the tests, we get the following compiler error

```
`std::cell::UnsafeCell<i32>` cannot be shared between threads safely
```

The reason we have to say get returns a copy is because otherwise we could
change the value invalidating the reference. In test form

```rust
fn bad2() {
    let x = Cell::new(String::from("hello"));
    let first = x.get();
    x.set(String::from("world"));
    assert_eq!(first, "hello");
}
```

The error we get here is that you cannot copy a string.

We can forcefully implement Sync for Cell to allow undefined behavior

```rust
unsafe impl<T> Sync for Cell<T> {}

impl<T> Cell<T> {
    // pub fn new ...

    pub fn set(&self, value: T) {
        unsafe { *self.value.get() = value };
    }

    pub fn get(&self) -> &T
    {
        unsafe { &*self.value.get() }
    }
}
```

Now we can run the tests to see the incorrect behavior

```shell
cargo test --lib bad2 -- --nocapture
```

The only way to correctly go from a shared reference to an exclusive reference
is using `UnsafeCell`.

### `RefCell`

https://doc.rust-lang.org/std/cell/struct.RefCell.html

`RefCell` is a mutable memory location with dynamically checked borrow rules.

`RefCell` is a way to get safe dynamic borrowing.

Our `RefCell` implementation will have an `UnsafeCell` value and a field
`reference` for counting the number of shared references

```rust
use std::cell::UnsafeCell;

enum RefState {
    Unshared,
    Shared(usize),
    Exclusive,
}

pub struct RefCell<T> {
    value: UnsafeCell<T>,
    state: RefState,
}

impl<T> RefCell<T> {
    pub fn new(value: T) -> Self {
        Self {
            value: UnsafeCell::new(value),
            state: RefState::Unshared,
        }
    }
}
```

The API we want to create is two functions:

1. `borrow` which gives a shared reference
   only if an exclusive reference doesn't exist
1. `borrow_mut` which gives an exclusive reference
   only if shared references don't exist

> The `Borrow` and `BorrowMut` traits are not related to this.

Here's an implementation of those functions

```rust
pub fn borrow(&self) -> Option<&T> {
    match self.state {
        RefState::Unshared => {
            self.state = RefState::Shared(1);
            Some(unsafe { &*self.value.get() })
        }
        RefState::Shared(n) => {
            self.state = RefState::Shared(n + 1);
            Some(unsafe { &*self.value.get() })
        }
        RefState::Exclusive => None,
    }
}

pub fn borrow_mut(&self) -> Option<&mut T> {
    if let RefState::Unshared = self.state {
        self.state = RefState::Exclusive;
        Some(unsafe { &mut *self.value.get() })
    } else {
        None
    }
}
```

But the issue is that we are trying to mutate `state` but we only have a shared
reference to `self`.

Another thing is that we are modifying the state in a way that's not thread
safe. We read the state's value at `RefState::Shared(n) => ...` and use that
value to increment the state. If two threads read the same value and increment
it, it's only going to increment by 1 instead of 2.

The second point is not an issue here because `UnsafeCell` implements `!Sync`
and therefore our `RefCell` does too.

To fix the mutation of state, we could use `Cell`!

```rust
use crate::cell::Cell;

// ...

pub struct RefCell<T> {
    value: UnsafeCell<T>,
    state: Cell<RefState>,
}
```

Notice that if we try to call `self.state.get()` the compiler complains about
`RefState` not being copyable. This can be fixed by adding a derive

```rust
#[derive(Copy, Clone)]
enum RefState {
    Unshared,
    Shared(usize),
    Exclusive,
}

// ...

impl<T> RefCell<T> {
    pub fn new(value: T) -> Self {
        Self {
            value: UnsafeCell::new(value),
            state: Cell::new(RefState::Unshared),
        }
    }

    pub fn borrow(&self) -> Option<&T> {
        match self.state.get() {
            RefState::Unshared => {
                self.state.set(RefState::Shared(1));
                Some(unsafe { &*self.value.get() })
            }
            RefState::Shared(n) => {
                self.state.set(RefState::Shared(n + 1));
                Some(unsafe { &*self.value.get() })
            }
            RefState::Exclusive => None,
        }
    }

    pub fn borrow_mut(&self) -> Option<&mut T> {
        if let RefState::Unshared = self.state.get() {
            self.state.set(RefState::Exclusive);
            Some(unsafe { &mut *self.value.get() })
        } else {
            None
        }
    }
}
```

But how about decreasing the number of shared borrows or going from exclusive to
unshared?

For this we can create our own reference type which decrements the refcell on
drop

```rust
pub struct Ref<'refcell, T> {
    refcell: &'refcell RefCell<T>,
}

impl<T> Drop for Ref<'_, T> {
    fn drop(&mut self) {
        match self.refcell.state.get() {
            RefState::Exclusive | RefState::Unshared => unreachable!(),
            RefState::Shared(1) => {
                self.refcell.state.set(RefState::Unshared);
            }
            RefState::Shared(n) => {
                self.refcell.state.set(RefState::Shared(n - 1));
            }
        }
    }
}
```

Our reference should also implement deref to be coherent with other reference
types. (It can be thought of as doing the -> operator from C automatically)

```rust
impl<T> std::ops::Deref for Ref<'_, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        unsafe { &*self.refcell.value.get() }
    }
}
```

The same for `RefMut`

```rust
pub struct RefMut<'refcell, T> {
    refcell: &'refcell RefCell<T>,
}

impl<T> std::ops::Deref for RefMut<'_, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        unsafe { &*self.refcell.value.get() }
    }
}

impl<T> std::ops::DerefMut for RefMut<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.refcell.value.get() }
    }
}

impl<T> Drop for RefMut<'_, T> {
    fn drop(&mut self) {
        match self.refcell.state.get() {
            RefState::Shared(_) | RefState::Unshared => unreachable!(),
            RefState::Exclusive => {
                self.refcell.state.set(RefState::Unshared);
            }
        }
    }
}
```

## `std::rc`

```text
Single-threaded reference-counting pointers. 'Rc' stands for 'Reference Counted'
```

`Rc` is similar to `RefCell` in the way it keeps count of shared and exclusive
references, but not similar in the sense that it doesn't provide mutability.

An `Rc` is deallocated when the last reference is dropped.

`Rc` is also `!Sync` and `!Send`.

An `Rc` doesn't work with cyclic references.

`Rc` needs to be stored on the heap, because many parts of the code might
reference it and therefore it cannot be on the stack of any given function.

What should the `Rc` be then?

1. It could be a `Box<T>`, but when you clone the `Rc` you clone the `Box` and
   cloning the `Box` clones the `T` which is not what we want...
1. It could be
   ```rust
   pub struct Rc<T> {
       value: *const T,
   }
   ```
   But then it doesn't track the reference count...
1. We could have `refcount: usize`
   ```rust
   pub struct Rc<T> {
       value: *const T,
       refcount: usize,
   }
   ```
   But then each clone of the `Rc` would have its own reference count...

Instead the reference count has to stored in the value that is shared accross
all the copies of the `Rc`.

```rust
struct RcInner<T> {
    value: T,
    refcount: usize,
}

pub struct Rc<T> {
    inner: *const RcInner<T>,
}

impl<T> Clone for Rc<T> {
    fn clone(&self) -> Self {
        let inner = &*self.inner;
        inner.refcount += 1;
        Rc { inner: self.inner }
    }
}
```

We need to implement `Deref` too

```rust
impl<T> std::ops::Deref for Rc<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        // SAFETY: self.inner is a Box that is deallocated only when the last Rc is dropped
        // we have an Rc, therefore the Box is still alive and dereferencing it is fine
        &unsafe { &*self.inner }.value
    }
}
```

How to initialize a new `Rc` with the `RcInner` allocated on the heap?

```rust
impl<T> Rc<T> {
    pub fn new(v: T) -> Self {
        let inner = Box::new(RcInner {
            value: v,
            refcount: 1,
        });

        Rc {
            // using &*inner won't work because inner would be
            // dropped and the reference would be invalid
            inner: Box::into_raw(inner),
        }
    }
}
```

At this stage in `clone` the compiler gives an error that we cannot mutate
refcount as we only have a shared reference to self.

The solution is `Cell` again!

```rust
struct RcInner<T> {
    value: T,
    refcount: Cell<usize>,
}

impl<T> Rc<T> {
    pub fn new(v: T) -> Self {
        let inner = Box::new(RcInner {
            value: v,
            refcount: Cell::new(1),
        });

        Rc {
            // using &*inner won't work because inner would be
            // dropped and the reference would be invalid
            inner: Box::into_raw(inner),
        }
    }
}

impl<T> Clone for Rc<T> {
    fn clone(&self) -> Self {
        let inner = &*self.inner;
        let c = inner.refcount.get();
        inner.refcount.set(c + 1);
        Rc { inner: self.inner }
    }
}
```

Now we also need to decrement the refcount using the same `Drop` solution like
with `RefCell`. Inside drop we need to decide whether to drop the inner value.

```rust
impl<T> Drop for Rc<T> {
    fn drop(&mut self) {
        let inner = unsafe {&*self.inner};
        let c = inner.refcount.get();
        if c == 1 {
            // SAFETY: we are the only Rc left, and therefore
            // dropping us means there are no more refences to
            drop(inner);
            let _ = unsafe { Box::from_raw(self.inner) };
        } else {
            // SAFETY: there are other Rcs so don't drop the Box
            inner.refcount.set(c - 1);
        }
    }
}
```

But the code doesn't compile because `Box::from_raw` expects a `*mut` rather
than a `*const`. Basically this is trying to say that you should take a raw
pointer that is shared and try to turn that into a `Box`. This ties into
_variance_ in rust
(https://doc.rust-lang.org/nightly/nomicon/subtyping.html#variance).

https://doc.rust-lang.org/std/ptr/struct.NonNull.html

`std::ptr::NonNull` can be used for performance reasons to tell the compiler
that a pointer cannot be null. `Option<NonNull<T>>` can be implemented without
overhead from the `Option`. It also provides a `as_ptr` to get the underlying
`*mut` pointer.

`NonNull` also implies that `Rc` must be `!Send`.

```rust
use std::ptr::NonNull;

pub struct Rc<T> {
    inner: NonNull<RcInner<T>>,
}

impl<T> Rc<T> {
    pub fn new(v: T) -> Self {
        let inner = Box::new(RcInner {
            value: v,
            refcount: Cell::new(1),
        });

        Rc {
            // SAFETY: Box never gives us a null pointer
            inner: unsafe { NonNull::new_unchecked(Box::into_raw(inner)) },
        }
    }
}
```

And also replace all `&*self.inner` with `self.inner.as_ref()`.

The difference between a mutable pointer and a mutable reference is that
a mutable reference guarantees that no one else is currently modifying it.
A mutable pointer does not guarantee that.

Note that in the `drop` implementation we are dropping `inner` before dropping
the `Box` because when we drop the `Box`, any pointer to that `Box` will be
invalid and `inner` is a pointer to that `Box`. The code would work without the
`drop(inner)` as it is dropped at the end of the scope, but nothing is stopping
someone from accidentally using inner after dropping the `Box` which might cause
UB.

### PhantomData and Drop Check

This test will fail

```rust
fn what() {
    let (y, x);
    x = String::from("foo");
    y = Rc::new(&x);
}
```

To fix it we need to add `PhantomData` markers to the `Rc`

```rust
use std::marker::PhantomData;

pub struct Rc<T> {
    inner: NonNull<RcInner<T>>,
    _marker: PhantomData<RcInner<T>>,
}
```

Without the `_marker` the compiler does not know that the type owns `T` and
therefore when the `Rc` is drop it doesn't know that the `T` is dropped as well.

Imagine a type that contains a reference and when it gets dropped it's going to
modify that reference.

The compiler must assume that dropping the type requires for it to exist until
the point of dropping.

```rust
struct Foo<'a, T: Default> {
    v: &'a mut T,
}
impl<T: Default> Drop for Foo<'_, T> {
    fn drop(&mut self) {
        std::mem::replace(self.v, T::default());
    }
}

fn main() {
    let (foo, mut t);
    t = String::from("hello");
    foo = Foo { v: &mut t }; // t does not live long enough
}
```

Problems might arise when t is dropped first, then foo is dropped and it tries
to access t so the compiler must check this (does the implementation of Foo's
drop do anything with t). This is known as the drop check.

https://doc.rust-lang.org/nightly/nomicon/dropck.html

The compiler can do the drop check only because it knows Foo holds a reference
to T.

Adding the marker tells the compiler that when you drop an `Rc`, an `RcInner<T>`
might be dropped and you need to check that. Without the marker the compiler
assumes this relation doesn't exist as it is only a pointer.

After adding the `PhantomData` markers the test compiles

```rust
fn what() {
    let (x, y);
    x = String::from("foo");
    y = Rc::new(&x);
}
```

Using `PhantomData<T>` would also work but then the `Drop` for `RcInner`
wouldn't be checked.

### `?Sized`

The real implementation of `Rc` allows `T` to be unsized `?Sized`. Rust normally
requires that every generic argument is `Sized`. This won't be implemented in
our version as it is quite complicated.

## Thread safety

### `RwLock`

https://doc.rust-lang.org/stable/std/sync/

A thread safe `RefCell` could use `Mutex` but it would have to be "checked" in
a loop. Instead `RwLock` allows reading when a writer doesn't have the lock and
when writing, it blocks the current thread until writing is available.

https://doc.rust-lang.org/stable/std/sync/struct.RwLock.html

`Mutex` can be thought of as a simpler version os `RwLock` because it only has
a `borrow_mut` method.

### `Arc`

`Arc` is an atomic `Rc`. It uses thread safe operations for managing the
reference counting rather than a `cell` like we did.

## `std::borrow::Cow`

https://doc.rust-lang.org/std/borrow/enum.Cow.html

`Cow` is sort of like a smart pointer.

`Cow`s have two variants, `Borrowed` and `Owned`

```rust
enum Cow<'a, B> {
    Borrowed(&'a B),
    Owned(<B as ToOwned>::Owned)
}
```

This means that a `Cow` either contains a reference to `B` or `B` itself.

If a `Cow` contains a shared reference, you can't modify the data. The point of
`Cow` is if you need write access to the value (like calling `get_mut`) and the
value is currently borrowed, it will clone the value and turn it into the
`Owned` version.

The place where `Cow` usually shows up is an operation where most of the time
you don't need to write and read is enough, but sometimes you need to modify it.

```rust
fn escape<'a>(s: &'a str) -> Cow<'a, str> {
    // ' -> \'
    // " -> \"
    // foo -> foo (no need to modify)
    if already_escaped(s) {
        Cow::Borrow(s)
    } else {
        let mut string = s.to_string();
        // escape string
        Cow::Owned(string)
    }
}
```

The reason `String`'s `from_utf8_lossy` returns a `Cow` is if the bytes are
valid utf-8, it can just cast it to a `&str`.

```rust
impl String {
    fn from_utf8_lossy(bytes: &[u8]) -> Cow<'_, str> {
        if valid_utf8(bytes) {
            Cow::Borrowed(bytes as &str)
        } else {
            let mut bts = Vec::from(bytes);
            for bts {
                // replace with INVALID_CHARACTER utf-8 symbol if invalid
            }
            Cow::Owned(bts as String)
        }
    }
}
```

<!-- vim: set sw=4 ts=4 tw=80 : -->
